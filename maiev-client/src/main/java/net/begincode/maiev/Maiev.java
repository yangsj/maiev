package net.begincode.maiev;

import net.begincode.maiev.bean.MonitorBean;
import net.begincode.maiev.message.Point;
import net.begincode.maiev.message.impl.DefaultPoint;
import net.begincode.maiev.monitor.BusinessMonitor;
import net.begincode.maiev.util.InterceptorUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 程序主入口
 * Created by Stay on 2017/1/6  22:42.
 */
public class Maiev {

    private static BusinessMonitor businessMonitor = BusinessMonitor.getInstance();

    private Maiev() {
    }


    /**
     * 埋点的创建
     *
     * @param methodName
     * @param requestType
     * @return
     */
    public static Point newPoint(String methodName, String requestType) {
        return new DefaultPoint(methodName, requestType);
    }

    public static Point newPoint() {
        return new DefaultPoint();
    }


    public static void trackMonitor(HttpServletRequest request, Object handler, long time) {
        trackMonitor(InterceptorUtils.getMethodName(handler), request.getMethod(), request.getRemoteHost(), time);

    }


    public static void trackMonitor(String completeName, String requestType, String remoteHost, Long time) {
        businessMonitor.recordMonitorData(completeName, time, requestType, remoteHost);
    }


    public static Map<String, MonitorBean> getMonitorData() {
        Map<String, MonitorBean> map = new HashMap<String, MonitorBean>();
        businessMonitor.getBusinessMonitorDataMap(map);
        return map;
    }



}
