package net.begincode.maiev.interceptor;

import net.begincode.maiev.Maiev;
import net.begincode.maiev.message.Point;
import net.begincode.maiev.util.InterceptorUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Stay on 2017/1/9  18:20.
 */
public class MaievInterceptor implements HandlerInterceptor {
    private ThreadLocal<Point> threadLocal = new ThreadLocal<>();


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Point point = Maiev.newPoint();
        threadLocal.set(point);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        Point point = threadLocal.get();
        point.addData(InterceptorUtils.getMethodName(handler), request.getMethod());
        point.complete();
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
