package net.begincode.maiev.message;

/**
 * Created by Stay on 2017/2/22  14:24.
 */
public interface Point {

    void addData(String name, String methodType);

    void complete();

}
