package net.begincode.maiev.servlet;

import com.alibaba.fastjson.JSON;
import net.begincode.maiev.Maiev;
import net.begincode.maiev.bean.MonitorBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 暴露给服务端的servlet接口
 *
 * Created by Stay on 2017/1/7  17:51.
 */
public class MonitorServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        Map<String,MonitorBean> map = Maiev.getMonitorData();
        PrintWriter out = response.getWriter();
        out.write(JSON.toJSONString(mapToList(map)));
        out.flush();
        out.close();
    }
    private List<MonitorBean> mapToList(Map<String,MonitorBean> map){
        List<MonitorBean> list = new ArrayList<>();
        Iterator<Map.Entry<String,MonitorBean>> iterator =  map.entrySet().iterator();
        while (iterator.hasNext()){
            list.add(iterator.next().getValue());
        }
        return list;
    }


}
