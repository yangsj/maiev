package net.begincode;

import com.alibaba.fastjson.JSON;
import net.begincode.maiev.bean.MonitorBean;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Stay on 2017/2/27  13:33.
 */
public class JsonTest {
    public static void main(String[] args){
        HashMap<String,MonitorBean> map = new HashMap();
        MonitorBean bean = new MonitorBean();
        bean.setMonitorName("aa");
        bean.setDate(new Date());
        bean.setHost("123.123.123.123");
        bean.setMaxRequestTime(new AtomicLong(11));
        bean.setRequestCount(new AtomicInteger(1));
        bean.setRequestType("get");
        bean.setTotalRequestTime(new AtomicLong(14));
        MonitorBean bean1 = new MonitorBean();
        bean1.setMonitorName("bb");
        bean1.setDate(new Date());
        bean1.setHost("123.123.123.123");
        bean1.setMaxRequestTime(new AtomicLong(11));
        bean1.setRequestCount(new AtomicInteger(1));
        bean1.setRequestType("get");
        bean1.setTotalRequestTime(new AtomicLong(14));
        map.put("aa", bean);
        map.put("bb", bean1);
        String ma = JSON.toJSONString(map);
        System.out.println(ma);
    }
}
