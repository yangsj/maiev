package net.begincode;

import net.begincode.maiev.Maiev;
import net.begincode.maiev.bean.MonitorBean;
import net.begincode.maiev.message.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Stay on 2017/1/16  16:35.
 */
public class MonitorTest {

    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    public static final int threadCount = 1000;

    public static void main(String[] args){
        List<Callable<Integer>> monitorList = new ArrayList<Callable<Integer>>();
        for(int i =0;i<threadCount;i++){
            monitorList.add(new Callable<Integer>() {
                public Integer call() throws Exception {
                    Point point = Maiev.newPoint("aa","post");
                    Thread.sleep(10);
                    point.complete();
                    return 1;
                }
            });
        }
        try {
            List<Future<Integer>> futures = executorService.invokeAll(monitorList);
            Map<String,MonitorBean> map  = Maiev.getMonitorData();
            for(Map.Entry entry : map.entrySet()){
                MonitorBean monitorBean = (MonitorBean) entry.getValue();
                System.out.println(entry.getKey()+":"+monitorBean.getTotalRequestTime()+" "+monitorBean.getRequestCount()+" "+monitorBean.getMaxRequestTime() +" " +monitorBean.getHost());
            }
            System.out.println("线程大小"+futures.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}
