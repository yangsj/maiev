package net.begincode.maiev.aspect;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.begincode.maiev.bean.Param;
import net.begincode.maiev.bean.Response;
import net.begincode.maiev.common.BizException;
import net.begincode.maiev.util.ResponseUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

/**
 * 传入controller 参数检查 param.check()
 * 如果使用@ResponseBody注解   就返回包装过的ajax
 * Created by Stay on 2017/3/3  12:08.
 */
@Aspect
@Component
@Order(1)
public class RequestAspect {

    private Logger logger = LoggerFactory.getLogger(RequestAspect.class);

    @Resource
    private HttpServletRequest request;

    @Pointcut("execution(* net.begincode.maiev.controller.*.*(..))")
    public void pointCut_() {
    }


    @Around("pointCut_()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) {
        Object[] objects = proceedingJoinPoint.getArgs();
        for (int i = 0; i < objects.length; i++) {
            if (objects[i] instanceof Param) {
                Param param = (Param) objects[i];
                param.check();
            }
        }
        MethodSignature joinPointObject = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = joinPointObject.getMethod();
        boolean flag = method.isAnnotationPresent(ResponseBody.class);
        Object returnObject = null;
        try {
            returnObject = proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            if (throwable instanceof BizException) {
                if (flag && method.getReturnType().equals(Void.TYPE)) {
                    ResponseUtils.exceptionResponse(logger, ((BizException) throwable).getStatus());
                } else if (flag && method.getReturnType().equals(Object.class)) {
                    ResponseUtils.exceptionResponse(logger, ((BizException) throwable).getStatus());
                } else {
                    writeContent(((BizException) throwable).getStatus().getMessage());
                    request.setAttribute("error", ((BizException) throwable).getStatus().getMessage());
                }
            }
        }
        if (flag && method.getReturnType().equals(Void.TYPE)) {
            voidResponse();
        }
        if (flag && !method.getReturnType().equals(Void.TYPE)) {
            return Response.success(returnObject);
        } else {
            return returnObject;
        }
    }

    private void voidResponse() {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        response.setHeader("Content-Type", "text/html;charset=UTF-8");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(response.getOutputStream(), Response.success());
        } catch (IOException e) {
            logger.error("Response error", e);
        }
    }


    /**
     * 将内容输出到浏览器
     *
     * @param content 输出内容
     */
    private void writeContent(String content) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        response.reset();
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Type", "text/plain;charset=UTF-8");
        response.setHeader("icop-content-type", "exception");
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.print(content);
        writer.flush();
        writer.close();
    }
}
