package net.begincode.maiev.bean;

/**
 * 邮件告警bean
 * Created by Stay on 2017/6/1  15:45.
 */
public class MailAlarmMessage {

    private String mailTitle;  //邮件标题

    private String mailSendName;  //邮件发送名

    private String monitorName;  //监控名

    private Integer groupFlag;   //是否是分组  0为不是 1为是

    private Long maxTime;   //超时最大毫秒

    private String projectName;  //告警项目名

    public String getMailTitle() {
        return mailTitle;
    }

    public void setMailTitle(String mailTitle) {
        this.mailTitle = mailTitle;
    }

    public String getMailSendName() {
        return mailSendName;
    }

    public void setMailSendName(String mailSendName) {
        this.mailSendName = mailSendName;
    }

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public Integer getGroupFlag() {
        return groupFlag;
    }

    public void setGroupFlag(Integer groupFlag) {
        this.groupFlag = groupFlag;
    }

    public Long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(Long maxTime) {
        this.maxTime = maxTime;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
