package net.begincode.maiev.bean;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 消费summary   转换类
 * <p>
 * Created by Stay on 2017/3/15  11:21.
 */
public class MonitorConvertBean {

    private AtomicInteger requestCount;

    private AtomicLong requestTotalTime;

    private AtomicLong requestMaxTime;

    private String host;

    private String requestType;

    private Integer projectId;

    private Date date;


    public MonitorConvertBean(AtomicInteger requestCount, AtomicLong requestTotalTime, String host, String requestType, Integer projectId) {
        this.requestCount = requestCount;
        this.requestTotalTime = requestTotalTime;
        this.host = host;
        this.requestType = requestType;
        this.projectId = projectId;
    }

    public MonitorConvertBean(AtomicInteger requestCount, AtomicLong requestTotalTime, AtomicLong requestMaxTime, String host, String requestType, Integer projectId) {
        this.requestCount = requestCount;
        this.requestTotalTime = requestTotalTime;
        this.requestMaxTime = requestMaxTime;
        this.host = host;
        this.requestType = requestType;
        this.projectId = projectId;
    }

    public MonitorConvertBean(AtomicInteger requestCount, AtomicLong requestTotalTime, AtomicLong requestMaxTime, String host, String requestType, Integer projectId, Date date) {
        this.requestCount = requestCount;
        this.requestTotalTime = requestTotalTime;
        this.requestMaxTime = requestMaxTime;
        this.host = host;
        this.requestType = requestType;
        this.projectId = projectId;
        this.date = date;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public AtomicInteger getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(AtomicInteger requestCount) {
        this.requestCount = requestCount;
    }

    public AtomicLong getRequestTotalTime() {
        return requestTotalTime;
    }

    public void setRequestTotalTime(AtomicLong requestTotalTime) {
        this.requestTotalTime = requestTotalTime;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public AtomicLong getRequestMaxTime() {
        return requestMaxTime;
    }

    public void setRequestMaxTime(AtomicLong requestMaxTime) {
        this.requestMaxTime = requestMaxTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void updateBean(Long time, int count, Long requestMaxTime) {
        addRequestCount(count);
        addTotalRequestTime(time);
        compareMaxRequestTime(requestMaxTime);
    }

    private void addRequestCount(int count) {
        requestCount.addAndGet(count);
    }


    private void addTotalRequestTime(Long time) {
        requestTotalTime.getAndAdd(time);
    }

    public void compareMaxRequestTime(Long time) {
        if (time > this.requestMaxTime.get()) {
            this.requestMaxTime.getAndSet(time);
        }
    }

    @Override
    public String toString() {
        return "MonitorConvertBean{" +
                "requestCount=" + requestCount +
                ", requestTotalTime=" + requestTotalTime +
                ", requestMaxTime=" + requestMaxTime +
                ", host='" + host + '\'' +
                ", requestType='" + requestType + '\'' +
                ", projectId=" + projectId +
                '}';
    }
}
