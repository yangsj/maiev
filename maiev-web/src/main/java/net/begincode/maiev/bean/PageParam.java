package net.begincode.maiev.bean;


/**
 * Created by Stay on 2016/9/19  11:43.
 */
public class PageParam extends Param {

    private int page; //传入的当前页号


    @Override
    public void check() {
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
