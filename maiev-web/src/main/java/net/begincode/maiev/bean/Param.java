package net.begincode.maiev.bean;


import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.ResponseEnum;
import net.begincode.maiev.util.ResponseUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yangsj on 2016/1/26.
 */
public abstract class Param {

    private Logger logger = LoggerFactory.getLogger(Param.class);

    public abstract void check();

    public void checkNotNull(Object value, ResponseEnum status) {
        checkArgs(value != null, status);
    }

    public void checkNotEmpty(String value, ResponseEnum status) {
        checkArgs(StringUtils.isNotBlank(value), status);
    }


    public void checkArgs(boolean success, ResponseEnum status) {
        if (!success) {
            ResponseUtils.exceptionResponse(logger, status);
            throw new BizException(status);
        }
    }
}
