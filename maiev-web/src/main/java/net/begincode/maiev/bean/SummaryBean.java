package net.begincode.maiev.bean;

import java.util.List;

/**
 * Created by Stay on 2017/3/1  15:30.
 */
public class SummaryBean {

    private Integer projectId;

    private List<MonitorBean> listBean;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public List<MonitorBean> getListBean() {
        return listBean;
    }

    public void setListBean(List<MonitorBean> listBean) {
        this.listBean = listBean;
    }
}
