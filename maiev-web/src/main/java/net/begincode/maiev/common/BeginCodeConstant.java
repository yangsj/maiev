package net.begincode.maiev.common;

/**
 * Created by Stay on 2016/9/16  19:22.
 */
public class BeginCodeConstant {
    public static final int PAGESIZE = 10;     //分页查询  每页显示的行数

    public static final String LOGINUSER = "loginUser";

    public static final String VERCODE = "verCode";
}
