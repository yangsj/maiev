package net.begincode.maiev.common;

import net.begincode.maiev.enums.ResponseEnum;

/**
 * Created by yangsj on 2016/8/20. 业务异常
 */
@SuppressWarnings("serial")
public class BizException extends RuntimeException {
	private ResponseEnum status;

	public BizException(ResponseEnum status) {
		super();
		this.status = status;
	}

	public ResponseEnum getStatus() {
		return status;
	}

	public void setStatus(ResponseEnum status) {
		this.status = status;
	}



}
