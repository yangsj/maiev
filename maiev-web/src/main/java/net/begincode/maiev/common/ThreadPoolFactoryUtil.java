package net.begincode.maiev.common;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 类名称：ThreadPoolFactoryUtil
 * 类描述：生成线程池的单例类
 * 创建人：bubble
 * 创建时间：2017年1月14日 上午9:59:25
 */

public class ThreadPoolFactoryUtil {
    private ExecutorService executorService;

    private ThreadPoolFactoryUtil() {
        //获取系统处理器个数，作为线程池数量
        int nThreads = Runtime.getRuntime().availableProcessors();
        executorService = Executors.newFixedThreadPool(nThreads);
    }

    //定义一个静态内部类创建本类对象
    private static class SingletonContainer {
        private static ThreadPoolFactoryUtil util = new ThreadPoolFactoryUtil();
    }

    //获取本类对象
    public static ThreadPoolFactoryUtil getUtil() {
        return SingletonContainer.util;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }
}
