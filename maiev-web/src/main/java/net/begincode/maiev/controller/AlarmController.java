package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.bean.Page;
import net.begincode.maiev.bean.PageParam;
import net.begincode.maiev.handler.AlarmHandler;
import net.begincode.maiev.handler.UserContext;
import net.begincode.maiev.model.AlarmFront;
import net.begincode.maiev.model.AlarmProjectSelect;
import net.begincode.maiev.model.AlarmSet;
import net.begincode.maiev.model.MaievUser;
import net.begincode.maiev.param.AlarmSaveParam;
import net.begincode.maiev.param.AlarmSetUpdateParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/4/13  20:16.
 */
@Controller
@RequestMapping(value = "/alarm")
public class AlarmController {
    @Resource
    private AlarmHandler alarmHandler;
    @Resource
    private UserContext userContext;

    @NeedLogin
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String alarm(Model model) {
        model.addAttribute("page_title", "告警设置");
        model.addAttribute("sidebar", "3");
        return "alarm";
    }

    @NeedLogin
    @RequestMapping(value = "/project", method = RequestMethod.GET)
    @ResponseBody
    public Object listAllAlarmProject() {
        MaievUser user = userContext.getCurrentUser();
        List<AlarmProjectSelect> AlarmProjectSelectList = alarmHandler.listAlarmProjectSelectByUserId(user.getId());
        return AlarmProjectSelectList;
    }

    @NeedLogin
    @RequestMapping(value = "/group/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public Object listAllAlarmGroup(@PathVariable("projectId") Integer projectId) {
        return alarmHandler.listGroupSelectByProjectId(projectId);
    }

    @NeedLogin
    @RequestMapping(value = "/item/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public Object listAllAlarmItem(@PathVariable("projectId") Integer projectId) {
        return alarmHandler.listItemSelectByProjectId(projectId);
    }


    @NeedLogin
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public void saveAlarm(AlarmSaveParam alarmSaveParam) {
        MaievUser user = userContext.getCurrentUser();
        AlarmSet alarmSet = alarmSaveParam.getAlarmSet();
        alarmSet.setUserId(user.getId());
        alarmHandler.saveAlarmSet(alarmSet);
    }

    @NeedLogin
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object listAlarmFont(PageParam pageParam) {
        MaievUser user = userContext.getCurrentUser();
        Page<AlarmFront> page = new Page<>();
        page.setCurrentNum(pageParam.getPage());
        alarmHandler.listAlarmByUserIdWithRowbounds(user.getId(), page);
        return page;
    }

    @NeedLogin
    @RequestMapping(value = "/{alarmId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getAlarmById(@PathVariable("alarmId") Integer id) {
        MaievUser user = userContext.getCurrentUser();
        AlarmFront alarmFontByIdAndUserId = alarmHandler.getAlarmFontByIdAndUserId(id, user.getId());
        return alarmFontByIdAndUserId;
    }


    @NeedLogin
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public void updateAlarm(AlarmSetUpdateParam alarmSetUpdateParam) {
        AlarmSet alarmSet = alarmSetUpdateParam.getAlarmSet();
        alarmHandler.updateAlarmSet(alarmSet);
    }

    @NeedLogin
    @RequestMapping(value = "/delete/{alarmId}", method = RequestMethod.GET)
    @ResponseBody
    public void deleteByAlarmId(@PathVariable("alarmId") Integer id) {
        MaievUser user = userContext.getCurrentUser();
        alarmHandler.deleteAlarmSetByIdAndUserId(id, user.getId());
    }


}
