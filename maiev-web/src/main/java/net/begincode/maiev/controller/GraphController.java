package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.handler.GraphHandler;
import net.begincode.maiev.handler.UserContext;
import net.begincode.maiev.model.MaievUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Stay on 2017/6/2  23:11.
 */
@Controller
@RequestMapping("/e")
public class GraphController {

    @Resource
    private GraphHandler graphHandler;
    @Resource
    private UserContext userContext;


    @NeedLogin
    @RequestMapping(value = "/describeMonitorData", method = RequestMethod.GET)
    @ResponseBody
    public Object echartsShow(@RequestParam("projectId") Integer projectId,
                              @RequestParam(value = "interval", defaultValue = "-1") Integer interval,
                              @RequestParam(value = "timeType", defaultValue = "hour") String timeType) {
        MaievUser currentUser = userContext.getCurrentUser();
        Calendar calendar = Calendar.getInstance();
        Date beginDate = null;
        Date endDate = new Date();
        if ("hour".equals(timeType)) {
            calendar.add(Calendar.HOUR_OF_DAY, interval);
            beginDate = calendar.getTime();
        }
        if("day".equals(timeType)){
            calendar.add(Calendar.DAY_OF_MONTH, interval);
            beginDate = calendar.getTime();
        }
        if("minute".equals(timeType)){
            calendar.add(Calendar.MINUTE,interval);
            beginDate = calendar.getTime();
        }
        return graphHandler.groupShow(beginDate, endDate, projectId, currentUser.getId());
    }
}
