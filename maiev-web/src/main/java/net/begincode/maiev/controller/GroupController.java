package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.bean.Page;
import net.begincode.maiev.bean.PageParam;
import net.begincode.maiev.handler.GroupHandler;
import net.begincode.maiev.model.BizMonitorGroup;
import net.begincode.maiev.model.MonitorGroup;
import net.begincode.maiev.param.MonitorGroupDeleteParam;
import net.begincode.maiev.param.MonitorGroupNameEditParam;
import net.begincode.maiev.param.MonitorGroupSaveParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by Stay on 2017/3/29  12:51.
 */
@Controller
@RequestMapping("/group")
public class GroupController {

    @Resource
    private GroupHandler groupHandler;

    @NeedLogin
    @RequestMapping(value = "/project/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public Object groupPageShow(@PathVariable("projectId") Integer projectId, PageParam pageParam) {
        Page<BizMonitorGroup> page = new Page<>();
        page.setCurrentNum(pageParam.getPage());
        groupHandler.groupPage(projectId, page);
        return page;
    }

    @NeedLogin
    @RequestMapping(value = "/url/{groupId}", method = RequestMethod.GET)
    @ResponseBody
    public Object groupUrl(@PathVariable("groupId") Integer groupId) {
        return groupHandler.listUrlByGroupId(groupId);
    }


    @NeedLogin
    @RequestMapping(value = "/item/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public Object groupItemShow(@PathVariable("projectId") Integer projectId, @RequestParam("p") Integer page) {
        return groupHandler.modalItemByProjectId(projectId, page);
    }

    @NeedLogin
    @RequestMapping(value = "/store", method = RequestMethod.POST)
    @ResponseBody
    public void groupAdd(MonitorGroupSaveParam monitorGroupParam) {
        MonitorGroup monitorGroup = monitorGroupParam.getMonitorGroup();
        int[] itemIdArray = monitorGroupParam.getItemIdArray();
        Date time = new Date();
        monitorGroup.setCreateTime(time);
        monitorGroup.setUpdateTime(time);
        groupHandler.saveGroupItem(monitorGroup, itemIdArray);
    }

    @NeedLogin
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public void groupEditName(MonitorGroupNameEditParam groupNameEditParam) {
        Integer id = groupNameEditParam.getId();
        String groupName = groupNameEditParam.getGroupName();
        groupHandler.editGroupNameById(id, groupName);
    }

    @NeedLogin
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public void groupDelete(MonitorGroupDeleteParam monitorGroupDeleteParam) {
        Integer id = monitorGroupDeleteParam.getId();
        groupHandler.deleteGroupById(id);
    }


}
