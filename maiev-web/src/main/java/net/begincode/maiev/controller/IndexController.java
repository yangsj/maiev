package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.bean.Page;
import net.begincode.maiev.bean.PageParam;
import net.begincode.maiev.handler.IndexHandler;
import net.begincode.maiev.handler.UserContext;
import net.begincode.maiev.model.MaievUser;
import net.begincode.maiev.model.MonitorProject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;


/**
 * Created by Stay on 2016/12/25  19:34.
 */
@Controller
@RequestMapping("/")
public class IndexController {

    @Resource
    private UserContext userContext;
    @Resource
    private IndexHandler indexHandler;


    @NeedLogin
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(Model model) {
        MaievUser user = userContext.getCurrentUser();
        Map<String, Integer> map = indexHandler.getIndexData(user.getId());
        model.addAttribute("page_title", "maiev监控");
        model.addAttribute("project_count", map.get("project_count"));
        model.addAttribute("url_count", map.get("url_count"));
        model.addAttribute("group_count", map.get("group_count"));
        model.addAttribute("sidebar", "1");
        return "index";
    }


    @NeedLogin
    @RequestMapping(value = "/projectList", method = RequestMethod.GET)
    @ResponseBody
    public Object projectPage(PageParam pageParam) {
        MaievUser maievUser = userContext.getCurrentUser();
        Page<MonitorProject> page = new Page<>();
        page.setCurrentNum(pageParam.getPage());
        indexHandler.projectStatusPage(page, maievUser.getId());
        return page;
    }



}
