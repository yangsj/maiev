package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.handler.ItemHandler;
import net.begincode.maiev.model.BizItem;
import net.begincode.maiev.param.GroupItemUpdateParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

import java.util.List;

/**
 * Created by Stay on 2017/4/4  20:34.
 */
@Controller
@RequestMapping("/item")
public class ItemController {
    @Resource
    private ItemHandler itemHandler;


    @NeedLogin
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public void groupItemEdit(GroupItemUpdateParam updateParam) {
        Integer groupId = updateParam.getGroupId();
        int[] itemIds = updateParam.getItemIds();
        itemHandler.updateGroupItem(groupId, itemIds);
    }


    @NeedLogin
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Object listNoInGroupItem(@RequestParam(value = "groupId", required = true) Integer groupId,
                                    @RequestParam(value = "projectId", required = true) Integer projectId,
                                    @RequestParam(value = "p", required = true) Integer current) {
        List<BizItem> list = itemHandler.listNoInGroupItemByGroupId(groupId, current, projectId);
        return list;
    }

    @NeedLogin
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public void groupItemDelete(@RequestParam(value = "groupId", required = true) Integer groupId,
                                @RequestParam(value = "itemId", required = true) Integer itemId) {
        itemHandler.deleteMonitorGroupItemByItemIdAndGroupId(itemId, groupId);
    }


}
