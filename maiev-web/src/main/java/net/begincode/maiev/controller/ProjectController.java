package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.bean.Page;
import net.begincode.maiev.bean.PageParam;
import net.begincode.maiev.handler.MonitorProjectHandler;
import net.begincode.maiev.handler.UserContext;
import net.begincode.maiev.model.BizFrontMonitorData;
import net.begincode.maiev.model.MaievUser;
import net.begincode.maiev.model.MonitorProject;
import net.begincode.maiev.param.CreateMonitorProjectParam;
import net.begincode.maiev.param.MonitorProjectUpdateParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by Stay on 2017/3/15  19:40.
 */
@Controller
@RequestMapping(value = "/project")
public class ProjectController {
    @Resource
    private MonitorProjectHandler monitorProjectHandler;
    @Resource
    private UserContext userContext;

    @NeedLogin
    @RequestMapping(value = "/sidebarProject", method = RequestMethod.GET)
    @ResponseBody
    public Object sidebarProject() {
        MaievUser maievUser = userContext.getCurrentUser();
        return monitorProjectHandler.listProject(maievUser.getId());
    }

    @NeedLogin
    @RequestMapping(value = "/{projectId}", method = RequestMethod.GET)
    public String project(@PathVariable("projectId") Integer projectId, Model model) {
        MonitorProject project = monitorProjectHandler.getMonitorProjectByProjectId(projectId);
        MaievUser maievUser = userContext.getCurrentUser();
        model.addAttribute("page_title", project.getProjectName() + "-监控详情");
        model.addAttribute("project", project);
        model.addAttribute("projectSidebar", monitorProjectHandler.listProject(maievUser.getId()));
        return "project";
    }


    @NeedLogin
    @RequestMapping(value = "/detail/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public Object monitorDetail(@PathVariable("projectId") Integer projectId, PageParam pageParam) {
        Page<BizFrontMonitorData> page = new Page<>();
        page.setCurrentNum(pageParam.getPage());
        monitorProjectHandler.listDetailByProjectId(projectId, page);
        return page;
    }


    @NeedLogin
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public void createProject(CreateMonitorProjectParam createProjectParam) {
        MaievUser user = userContext.getCurrentUser();
        MonitorProject monitorProject = createProjectParam.getMonitorProject();
        monitorProject.setCreateTime(new Date());
        monitorProject.setUserId(user.getId());
        String[] grabUrlNames = createProjectParam.getGrabUrlNames();
        monitorProjectHandler.saveProjectAndGrabUrl(monitorProject, grabUrlNames);
    }


    @NeedLogin
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteProject(@RequestParam(value = "projectId", required = true) Integer projectId) {
        MaievUser user = userContext.getCurrentUser();
        monitorProjectHandler.deleteMonitorProjectByUserIdAndProId(user.getId(), projectId);
    }

    @NeedLogin
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public void updateProject(@RequestParam(value = "projectId", required = true) Integer projectId, MonitorProjectUpdateParam monitorProjectUpdateParam) {
        MaievUser user = userContext.getCurrentUser();
        MonitorProject monitorProject = new MonitorProject();
        monitorProject.setProjectName(monitorProjectUpdateParam.getProjectName());
        monitorProject.setNote(monitorProjectUpdateParam.getNote());
        String[] urls = monitorProjectUpdateParam.getUrls();
        monitorProjectHandler.updateMonitorProject(monitorProject, projectId, user.getId(), urls);
    }

    @NeedLogin
    @RequestMapping(value = "/modal/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public Object projectModalShow(@PathVariable(value = "projectId") Integer projectId) {
        return monitorProjectHandler.getMonitorProjectByProjectId(projectId);
    }


}
