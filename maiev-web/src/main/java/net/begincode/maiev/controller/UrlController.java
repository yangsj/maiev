package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.handler.UrlHandler;
import net.begincode.maiev.param.MonitorUrlDeleteParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by Stay on 2017/4/8  16:05.
 */
@Controller
@RequestMapping(value = "/url")
public class UrlController {

    @Resource
    private UrlHandler urlHandler;


    @NeedLogin
    @RequestMapping(value = "/project/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public Object showUrlByProjectId(@PathVariable Integer projectId) {
        return urlHandler.listMonitorProjectUrlByProjectId(projectId);
    }

    @NeedLogin
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteUrl(MonitorUrlDeleteParam monitorUrlDeleteParam) {
        String urlName = monitorUrlDeleteParam.getUrlName();
        Integer projectId = monitorUrlDeleteParam.getProjectId();
        urlHandler.removeMonitorProjectUrlByName(urlName,projectId);
    }

}
