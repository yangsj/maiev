package net.begincode.maiev.controller;

import net.begincode.maiev.annotation.NeedLogin;
import net.begincode.maiev.common.BeginCodeConstant;
import net.begincode.maiev.handler.UserContext;
import net.begincode.maiev.model.MaievUser;
import net.begincode.maiev.param.MaievUserCheckParam;
import net.begincode.maiev.param.MaievUserLoginParam;
import net.begincode.maiev.param.MaievUserRegParam;
import net.begincode.maiev.util.VerifyCodeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Liu
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserContext userContext;

    @NeedLogin
    @RequestMapping(value="man",method = RequestMethod.GET)
    public String manage(){
        return "userman";
    }

    // 用户注册
    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    @ResponseBody
    public void reg(MaievUserRegParam maievUserRegParam) {
        userContext.regist(maievUserRegParam.getMaievUser());
    }

    // 用户退出登录
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String loginOut() {
        userContext.loginOut();
        return "login";
    }

    /**
     * 用户登录
     *
     * @param maievUserLoginParam
     */
    @RequestMapping(value = "/sign", method = RequestMethod.POST)
    @ResponseBody
    public void login(MaievUserLoginParam maievUserLoginParam) {
        userContext.login(maievUserLoginParam.getMaievUser());
    }


    /**
     * 登录页面跳转
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String sign(Model model) {
        model.addAttribute("page_title", "maiev登录");
        return "login";
    }

    /**
     * 注册页面跳转
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("page_title", "maiev注册");
        return "register";
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    @ResponseBody
    public void checkParam(MaievUserCheckParam maievUserCheckParamm) {
        String checkName = maievUserCheckParamm.getCheckName();
        MaievUser user = maievUserCheckParamm.getMaievUser();
        String code = maievUserCheckParamm.getCode();
        switch (checkName) {
            case "username":
                userContext.checkUserName(user.getUsername());
                break;
            case "nickname":
                userContext.checkNickName(user.getNickname());
                break;
            case "email":
                userContext.checkEmail(user.getEmail());
                break;
            case "code":
                userContext.checkCode(code);
                break;
        }
    }


    /**
     * 验证码图片输出  verCode存入session中
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/authImage", method = RequestMethod.GET)
    public void authImage(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("image/jpeg");
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        HttpSession session = request.getSession(true);
        //删除以前的
        session.removeAttribute(BeginCodeConstant.VERCODE);
        //存入code
        session.setAttribute(BeginCodeConstant.VERCODE, verifyCode.toLowerCase());
        int w = 100;
        int h = 30;
        try {
            VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), verifyCode);
        } catch (IOException e) {
            //ignore
        }
    }


}
