package net.begincode.maiev.enums;

/**
 * Created by Stay on 2017/3/13  17:36.
 */
public enum HandleFlagEnum {
    IS_HANDLE(1, "已处理"), NO_HANDLE(0, "未处理");
    private Integer code;
    private String message;

    HandleFlagEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
