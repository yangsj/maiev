package net.begincode.maiev.enums;

/**
 * Created by Stay on 2017/5/25  16:15.
 */
public enum ReadFlagEnum {
    IS_READ(1,"已阅读"),NO_READ(0,"未阅读");
    private Integer code;
    private String message;

    ReadFlagEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
