package net.begincode.maiev.enums;

/**
 * Created by Stay on 2017/6/11  19:49.
 */
public enum RequestAuthEnum implements ResponseEnum {
    NO_PERMISSIONS("8","权限认证失败");

    private String code;
    private String message;

    RequestAuthEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
