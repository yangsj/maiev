package net.begincode.maiev.enums;

/**
 * Created by Stay on 2017/3/17  15:47.
 */
public enum StatusFlagEnum implements ResponseEnum {
    UNKNOWN_STATUS("0", "未知"),
    NORMAL_STATUS("1", "正常"),
    ABNORMAL_STATUS("2", "异常");
    private String code;
    private String message;

    StatusFlagEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
