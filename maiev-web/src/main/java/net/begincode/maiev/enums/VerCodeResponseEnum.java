package net.begincode.maiev.enums;

/**
 * Created by Stay on 2017/3/2  19:25.
 */
public enum VerCodeResponseEnum implements ResponseEnum {
    VERCODE_VALIDATE_ERROR("7", "验证码验证错误");

    private String code;
    private String message;

    VerCodeResponseEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
