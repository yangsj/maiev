package net.begincode.maiev.handler;

import net.begincode.maiev.bean.Page;
import net.begincode.maiev.common.BeginCodeConstant;
import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.*;
import net.begincode.maiev.monitor.mail.AlarmSetObserver;
import net.begincode.maiev.monitor.mail.SubjectOperation;
import net.begincode.maiev.service.AlarmProjectSelectService;
import net.begincode.maiev.service.AlarmSetService;
import net.begincode.maiev.service.MonitorGroupService;
import net.begincode.maiev.service.MonitorItemService;
import net.begincode.maiev.util.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stay on 2017/4/25  11:16.
 */
@Component
public class AlarmHandler {
    private static final Logger logger = LoggerFactory.getLogger(AlarmHandler.class);

    @Resource
    private AlarmProjectSelectService alarmProjectSelectService;
    @Resource
    private AlarmSetService alarmSetService;
    @Resource
    private MonitorGroupService monitorGroupService;
    @Resource
    private MonitorItemService monitorItemService;

    /**
     * @param userId
     * @return
     */
    public List<AlarmProjectSelect> listAlarmProjectSelectByUserId(Integer userId) {
        List<AlarmProjectSelect> list = alarmProjectSelectService.listAlarmProjectSelectByUserId(userId);
        if (ListUtils.isNull(list)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return list;
    }

    /**
     * 分页展示告警
     *
     * @param userId
     * @param page
     */
    public void listAlarmByUserIdWithRowbounds(Integer userId, Page page) {
        List<AlarmSet> alarmSets = alarmSetService.listAlarmByUserIdWithRowbounds(userId, page.getCurrentNum(), BeginCodeConstant.PAGESIZE);
        if (ListUtils.isNull(alarmSets)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        List<AlarmFront> alarmFrontList = new ArrayList<>(alarmSets.size());
        AlarmSet alarmSet = null;
        AlarmFront alarmFront = null;
        for (int i = 0; i < alarmSets.size(); i++) {
            alarmSet = alarmSets.get(i);
            alarmFront = new AlarmFront();
            if (alarmSet.getGroupId() == 0) {
                String monitorItemName = monitorItemService.getMonitorItemById(alarmSet.getMonitorItemId()).getMonitorItemName();
                alarmFront.setAlarmName(monitorItemName);
                alarmFront.setId(alarmSet.getId());
                alarmFront.setQpsThreshold(alarmSet.getQpsThreshold());
                alarmFront.setTimeThreshold(alarmSet.getTimeThreshold());
            } else {
                String groupName = monitorGroupService.getMonitorGroupById(alarmSet.getGroupId()).getGroupName();
                alarmFront.setAlarmName(groupName);
                alarmFront.setId(alarmSet.getId());
                alarmFront.setQpsThreshold(alarmSet.getQpsThreshold());
                alarmFront.setTimeThreshold(alarmSet.getTimeThreshold());
            }
            alarmFrontList.add(alarmFront);
        }
        page.setTotalNum(alarmSetService.countAlarmSetByUserId(userId));
        page.setData(alarmFrontList);
    }

    /**
     * 通过项目id 得到分组选择
     *
     * @param projectId
     * @return
     */
    public List<AlarmGroupSelect> listGroupSelectByProjectId(Integer projectId) {
        List<AlarmGroupSelect> list = alarmProjectSelectService.listGroupSelectByProjectId(projectId);
        if (ListUtils.isNull(list)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return list;
    }

    /**
     * 通过项目id 得到监控项选择
     * 没有数据则抛出数据不存在
     *
     * @param projectId
     * @return
     */
    public List<AlarmItemSelect> listItemSelectByProjectId(Integer projectId) {
        List<AlarmItemSelect> list = alarmProjectSelectService.listItemSelectByProjectId(projectId);
        if (ListUtils.isNull(list)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return list;
    }

    /**
     * 存储告警
     *
     * @param alarmSet
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveAlarmSet(AlarmSet alarmSet) {
        try {
            alarmSetService.saveAlarmSet(alarmSet);
            //添加进告警容器
            AlarmSetObserver alarmSetObserver = new AlarmSetObserver(alarmSet);
            SubjectOperation.getInstance().addObserver(alarmSetObserver);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }

    /**
     * 更新告警
     *
     * @param alarmSet
     */
    public void updateAlarmSet(AlarmSet alarmSet) {
        try {
            alarmSetService.updateByAlarmSet(alarmSet);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }

    /**
     * 得到前台告警
     *
     * @param id
     * @param userId
     * @return
     */
    public AlarmFront getAlarmFontByIdAndUserId(Integer id, Integer userId) {
        AlarmSet alarmSet = alarmSetService.getAlarmSetById(id, userId);
        if (alarmSet == null) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        AlarmFront alarmFront = new AlarmFront();
        if (alarmSet.getGroupId() == 0) {
            String monitorItemName = monitorItemService.getMonitorItemById(alarmSet.getMonitorItemId()).getMonitorItemName();
            alarmFront.setAlarmName(monitorItemName);
        } else {
            String groupName = monitorGroupService.getMonitorGroupById(alarmSet.getGroupId()).getGroupName();
            alarmFront.setAlarmName(groupName);
        }
        alarmFront.setId(alarmSet.getId());
        alarmFront.setTimeThreshold(alarmSet.getTimeThreshold());
        alarmFront.setQpsThreshold(alarmSet.getQpsThreshold());
        return alarmFront;
    }

    /**
     * 删除AlarmSet
     *
     * @param id
     * @param userId
     */
    public void deleteAlarmSetByIdAndUserId(Integer id, Integer userId) {
        try {
            alarmSetService.deleteAlarmSetByIdAndUserId(id, userId);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
    }


}
