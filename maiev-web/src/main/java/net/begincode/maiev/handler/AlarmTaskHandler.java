package net.begincode.maiev.handler;

import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.AlarmRecord;
import net.begincode.maiev.model.AlarmSet;
import net.begincode.maiev.model.MonitorGroupItem;
import net.begincode.maiev.model.MonitorItem;
import net.begincode.maiev.service.AlarmRecordService;
import net.begincode.maiev.service.AlarmSetService;
import net.begincode.maiev.service.MonitorGroupItemService;
import net.begincode.maiev.service.MonitorItemService;
import net.begincode.maiev.util.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/5/25  20:54.
 */
@Component
public class AlarmTaskHandler {

    private static final Logger logger = LoggerFactory.getLogger(AlarmTaskHandler.class);

    @Resource
    private AlarmSetService alarmSetService;
    @Resource
    private MonitorGroupItemService monitorGroupItemService;
    @Resource
    private MonitorItemService monitorItemService;
    @Resource
    private AlarmRecordService alarmRecordService;


    public List<AlarmSet> listAllAlarmSet() {
        List<AlarmSet> alarmSets = alarmSetService.listAllAlarmSet();
        if (ListUtils.isNull(alarmSets)) {
            return null;
        }
        return alarmSets;
    }

    public MonitorItem getMonitorItemById(Integer id) {
        MonitorItem monitorItem = monitorItemService.getMonitorItemById(id);
        return monitorItem;
    }

    public List<MonitorGroupItem> listMonitorGroupItemByGroupId(Integer groupId) {
        return monitorGroupItemService.listMonitorGroupItemByGroupId(groupId);
    }

    /**
     * 保存用户告警内容
     *
     * @param alarmRecord
     */
    public void saveAlarmRecord(AlarmRecord alarmRecord) {
        try {
            alarmRecordService.saveAlarmRecord(alarmRecord);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }

}
