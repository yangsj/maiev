package net.begincode.maiev.handler;

import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.enums.RequestAuthEnum;
import net.begincode.maiev.model.MonitorProject;
import net.begincode.maiev.service.BizMonitorEchartsService;
import net.begincode.maiev.service.MonitorGroupService;
import net.begincode.maiev.service.MonitorProjectService;
import net.begincode.maiev.util.ObjectUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by Stay on 2017/6/11  17:14.
 */
@Component
public class GraphHandler {

    @Resource
    private MonitorProjectService monitorProjectService;
    @Resource
    private BizMonitorEchartsService bizMonitorEchartsService;

    public Object groupShow(Date beginTime, Date endTime, Integer projectId, Integer userId) {
        if (ObjectUtils.isNull(beginTime)) {
            throw new BizException(CommonResponseEnum.PARAM_ERROR);
        }
        List<MonitorProject> projectList = monitorProjectService.getProjectByUserId(userId);
        int length = projectList.size();
        for (int i = 0; i < length; i++) {
            if (projectList.get(i).getId() == projectId) {
                return bizMonitorEchartsService.listEchartsDataByProjectId(projectId, beginTime, endTime);
            }
        }
        throw new BizException(RequestAuthEnum.NO_PERMISSIONS);
    }
}
