package net.begincode.maiev.handler;

import net.begincode.maiev.bean.Page;
import net.begincode.maiev.common.BeginCodeConstant;
import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.*;
import net.begincode.maiev.model.*;
import net.begincode.maiev.service.*;
import net.begincode.maiev.util.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/3/29  12:51.
 */
@Component
public class GroupHandler {

    private Logger logger = LoggerFactory.getLogger(GroupHandler.class);

    @Resource
    private BizMonitorGroupService bizMonitorGroupService;
    @Resource
    private BizItemService bizItemService;
    @Resource
    private BizMonitorGroupEchartService bizMonitorGroupEchartService;
    @Resource
    private MonitorItemService monitorItemService;
    @Resource
    private MonitorGroupService monitorGroupService;
    @Resource
    private MonitorGroupItemService monitorGroupItemService;

    public void groupPage(Integer projectId, Page<BizMonitorGroup> page) {
        List<BizMonitorGroup> groupList = bizMonitorGroupService.listGroupByProjectId(projectId, page.getCurrentNum(), BeginCodeConstant.PAGESIZE);
        if (ListUtils.isNull(groupList)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        page.setTotalNum(bizMonitorGroupService.countGroupByProjectId(projectId));
        page.setData(groupList);
    }

    public List<BizItem> listUrlByGroupId(Integer groupId) {
        List<BizItem> groupUrlList = bizItemService.listUrlByGroupId(groupId);
        if (ListUtils.isNull(groupUrlList)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return groupUrlList;
    }

    public List<BizMonitorGroupEchart> groupEchartDataByGroupId(Integer groupId) {
        List<BizMonitorGroupEchart> list = bizMonitorGroupEchartService.listByGroupId(groupId);
        if (ListUtils.isNull(list)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return list;
    }

    public List<MonitorItem> modalItemByProjectId(Integer projectId, Integer scollNum) {
        List<MonitorItem> list = monitorItemService.listMonitorItemWithRowbounds(projectId, scollNum, BeginCodeConstant.PAGESIZE);
        if (ListUtils.isNull(list)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return list;
    }

    public void saveGroupItem(MonitorGroup monitorGroup, int[] itemId) {
        try {
            monitorGroupService.saveMonitorGroup(monitorGroup);
            MonitorGroupItem groupItem = null;
            for (int i = 0; i < itemId.length; i++) {
                groupItem = new MonitorGroupItem();
                groupItem.setGroupId(monitorGroup.getId());
                groupItem.setMonitorItemId(itemId[i]);
                monitorGroupItemService.saveMonitorGroupItem(groupItem);
            }
        } catch (Exception e) {
            logger.error(CommonResponseEnum.DATA_NULL.getMessage(), e);
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
    }

    public void editGroupNameById(Integer id, String groupName) {
        try {
            MonitorGroup monitorGroup = new MonitorGroup();
            monitorGroup.setId(id);
            monitorGroup.setGroupName(groupName);
            monitorGroupService.updateGroup(monitorGroup);
        } catch (Exception e) {
            logger.error(CommonResponseEnum.EXCEPTION.getMessage(), e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }

    public void deleteGroupById(Integer id){
        try {
            monitorGroupService.deleteMonitorGroupById(id);
        }catch (Exception e){
            logger.error(CommonResponseEnum.EXCEPTION.getMessage(),e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }




}
