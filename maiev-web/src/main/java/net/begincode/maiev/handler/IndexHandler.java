package net.begincode.maiev.handler;

import net.begincode.maiev.bean.Page;
import net.begincode.maiev.model.MonitorProject;
import net.begincode.maiev.model.MonitorProjectUser;
import net.begincode.maiev.model.ProjectOverview;
import net.begincode.maiev.service.*;
import net.begincode.maiev.util.ListUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

import static com.sun.tools.classfile.Opcode.get;

/**
 * 首页数据查询
 * 事务层
 * Created by Stay on 2017/2/27  16:38.
 */
@Component
public class IndexHandler {

    @Resource
    private MonitorProjectService monitorProjectService;
    @Resource
    private MonitorProjectUrlService monitorProjectUrlService;
    @Resource
    private MonitorGroupService monitorGroupService;
    @Resource
    private ProjectOverviewService projectOverviewService;
    @Resource
    private MonitorSummaryService monitorSummaryService;
    @Resource
    private MonitorProjectUserService monitorProjectUserService;


    /**
     * 查询得到主页部分数据
     *
     * @param userId
     * @return
     */
    public Map<String, Integer> getIndexData(Integer userId) {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("project_count", monitorProjectService.countByUserId(userId));
        List<Integer> projectIdList = changeToProjectIdList(monitorProjectService.listMonitorProjectByUserId(userId));
        if (ListUtils.isNull(projectIdList)) {
            map.put("url_count", 0);
            map.put("group_count", 0);
        } else {
            map.put("url_count", monitorProjectUrlService.countProjectUrl(projectIdList));
            map.put("group_count", monitorGroupService.countGroupByProjectIdList(projectIdList));
        }
        return map;
    }


    public List<ProjectOverview> listProjectOverviewByPage(Integer current, Integer pageSize) {
        return projectOverviewService.listProjectOverviewByPage(current, pageSize);
    }


    public Integer countProjectByUserId(Integer userId) {
        return monitorProjectService.countByUserId(userId);
    }

    /**
     * (前台项目状态)
     * 填充分页Page参数
     *
     * @param page
     * @param userId
     */
    public void projectStatusPage(Page<MonitorProject> page, Integer userId) {
        List<MonitorProject> list = listMonitorProjectByPage(page.getCurrentNum(), page.getPageEachSize(), userId);
        page.setData(list);
        page.setTotalNum(countProjectByUserId(userId));
    }


    /**
     * 监控项目分页
     *
     * @param current  当前页
     * @param pageSize 一页显示多少条数据
     * @param userId   用户id
     * @return
     */
    public List<MonitorProject> listMonitorProjectByPage(Integer current, Integer pageSize, Integer userId) {
        List<MonitorProject> monitorProjectList = monitorProjectService.listMonitorProjectWithRowbounds(current, pageSize, userId);
        if (ListUtils.isNull(monitorProjectList)) {
            List<MonitorProjectUser> monitorProjectUsers = monitorProjectUserService.listMonitorProjectUserByUserId(userId, current, pageSize);
            if (!ListUtils.isNull(monitorProjectUsers)) {
                List<Integer> projectIds = new ArrayList<>(monitorProjectUsers.size());
                for (int i = 0; i < monitorProjectUsers.size(); i++) {
                    projectIds.add(monitorProjectUsers.get(i).getMonitorProjectId());
                }
                return monitorProjectService.listMonitorProjectByIdList(projectIds);
            }
        }
        return monitorProjectService.listMonitorProjectWithRowbounds(current, pageSize, userId);
    }

    /**
     * 传入项目id  和某个时间
     * 得到: 时间-1分钟之间的数据集合
     *
     * @param date   Date类型
     * @param userId 用户Id
     * @return
     */
    public Integer countMonitorSummary(Date date, Integer userId) {
        List<MonitorProject> projectList = monitorProjectService.listMonitorProjectByUserId(userId);
        List<Integer> projectIdList = changeToProjectIdList(projectList);
        return monitorSummaryService.countMonitorSummaryByDate(date, projectIdList);
    }

    /**
     * 传入项目集合 返回项目标识集合
     *
     * @param list
     * @return
     */
    private List<Integer> changeToProjectIdList(List<MonitorProject> list) {
        List<Integer> projectIdList = null;
        if (!ListUtils.isNull(list)) {
            projectIdList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                projectIdList.add(list.get(i).getId());
            }
            return projectIdList;
        }
        return null;
    }

}
