package net.begincode.maiev.handler;

import net.begincode.maiev.bean.Page;
import net.begincode.maiev.common.BeginCodeConstant;
import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.*;
import net.begincode.maiev.service.*;
import net.begincode.maiev.util.ListUtils;
import net.begincode.maiev.util.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Stay on 2017/3/15  19:40.
 */
@Component
public class MonitorProjectHandler {
    @Resource
    private MonitorProjectService monitorProjectService;
    @Resource
    private BizFrontMonitorDataService bizFrontMonitorDataService;
    @Resource
    private BizMonitorCharDataService bizMonitorCharDataService;
    @Resource
    private MonitorProjectUrlService monitorProjectUrlService;
    @Resource
    private BizMonitorProjectUrlService bizMonitorProjectUrlService;
    @Resource
    private MonitorProjectUserService monitorProjectUserService;


    private Logger logger = LoggerFactory.getLogger(MonitorProjectHandler.class);


    /**
     * 通过用户标识 返回BizMonitorProject集合
     *
     * @param userId
     * @return
     */
    public List<SideBarMonitorProject> listProject(Integer userId) {
        List<MonitorProject> projectList = monitorProjectService.listMonitorProjectByUserId(userId);
        SideBarMonitorProject sideBarMonitorProject = null;
        MonitorProject project = null;
        List<SideBarMonitorProject> sideProjectList = new ArrayList<>(projectList.size());
        if (!ListUtils.isNull(projectList)) {
            for (int i = 0; i < projectList.size(); i++) {
                project = projectList.get(i);
                sideBarMonitorProject = new SideBarMonitorProject();
                sideBarMonitorProject.setProjectName(project.getProjectName());
                sideBarMonitorProject.setProjectId(project.getId());
                sideProjectList.add(sideBarMonitorProject);
            }
        }
        return sideProjectList;
    }

    public void updateMonitorProjectStatus(MonitorProject monitorProject) {
        try {
            monitorProjectService.updateProjectStatus(monitorProject);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }


    public MonitorProject getMonitorProjectByProjectId(Integer projectId) {
        MonitorProject project = monitorProjectService.getMonitorProjectByProjectId(projectId);
        if (StringUtils.isEmpty(project)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return project;
    }

    /**
     * 前台监控url 数据分页展示
     *
     * @param projectId
     * @param page
     * @return
     */
    public void listDetailByProjectId(Integer projectId, Page<BizFrontMonitorData> page) {
        List<BizFrontMonitorData> monitorDataList = bizFrontMonitorDataService.listFrontMonitorDataByProjectIdWithPage(projectId, page.getCurrentNum(), BeginCodeConstant.PAGESIZE);
        if (!ListUtils.isNull(monitorDataList)) {
            page.setTotalNum(bizFrontMonitorDataService.countFrontMonitorDataByProjectId(projectId));
            page.setData(monitorDataList);
        } else {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
    }

    /**
     * 单个url 图表变化
     *
     * @param monitorName
     * @return
     */
    public List<BizMonitorChartData> urlMonitorEchartShow(String monitorName) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date beginDate = calendar.getTime();
        Date endDate = new Date();
        List<BizMonitorChartData> chartDataList = bizMonitorCharDataService.listChartDataByDate(monitorName, beginDate, endDate);
        if (ListUtils.isNull(chartDataList)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return chartDataList;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveProjectAndGrabUrl(MonitorProject monitorProject, String[] grabUrlNames) {
        List<MonitorProjectUrl> list = new ArrayList<>();
        MonitorProjectUrl projectUrl = null;
        try {
            monitorProjectService.saveMonitorProject(monitorProject);
            for (int i = 0; i < grabUrlNames.length; i++) {
                if (!StringUtils.isEmpty(grabUrlNames[i])) {
                    projectUrl = new MonitorProjectUrl();
                    projectUrl.setProjectId(monitorProject.getId());
                    projectUrl.setMonitorUrl(grabUrlNames[i]);
                    list.add(projectUrl);
                }
            }
            bizMonitorProjectUrlService.saveBatchByMonitorProjectUrlList(list);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteMonitorProjectByUserIdAndProId(Integer userId, Integer projectId) {
        try {
            monitorProjectService.deleteMonitorProjectByUserIdAndProId(userId, projectId);
            monitorProjectUrlService.removeProjectUrlByProId(projectId);
            monitorProjectUserService.deleteByProjectIdAndUserId(projectId,userId);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }

    public void updateMonitorProject(MonitorProject monitorProject, Integer projectId, Integer userId, String[] urls) {
        try {
            MonitorProjectUrl monitorProjectUrl = null;
            for (int i = 0; i < urls.length; i++) {
                if (!StringUtils.isEmpty(urls[i])) {
                    monitorProjectUrl = monitorProjectUrlService.getProjectUrlByName(urls[i]);
                    if (ObjectUtils.isNull(monitorProjectUrl)) {
                        monitorProjectUrl = new MonitorProjectUrl();
                        monitorProjectUrl.setMonitorUrl(urls[i]);
                        monitorProjectUrl.setProjectId(projectId);
                        monitorProjectUrlService.saveMonitorUrl(monitorProjectUrl);
                    }
                }
            }
            monitorProjectService.updateMonitorProject(monitorProject, projectId, userId);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }


}
