package net.begincode.maiev.handler;

import net.begincode.maiev.bean.MonitorConvertBean;
import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.MonitorDetail;
import net.begincode.maiev.model.MonitorItem;
import net.begincode.maiev.model.MonitorProjectUrl;
import net.begincode.maiev.model.MonitorSummary;
import net.begincode.maiev.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/3/15  11:44.
 */
@Component
public class SummaryTaskHandler {
    private Logger logger = LoggerFactory.getLogger(SummaryTaskHandler.class);
    @Resource
    private MonitorSummaryService monitorSummaryService;
    @Resource
    private MonitorItemService monitorItemService;
    @Resource
    private MonitorProjectService monitorProjectService;
    @Resource
    private MonitorDetailService monitorDetailService;
    @Resource
    private MonitorProjectUrlService monitorProjectUrlService;

    /**
     * 查询指定大小未处理的数据出来
     *
     * @param size
     * @return
     */
    public List<MonitorSummary> listNoHandleSummaryBySize(Integer size) {
        return monitorSummaryService.listNoHandleSummaryBySize(size);
    }

    /**
     * 查询当前时间一分钟前未处理的数据
     *
     * @return
     */
    public List<MonitorSummary> listMinuteAgoNoHandleSumary() {
        return monitorSummaryService.listMinuteAgoNoHandleSumary();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateHandleFlagSummary(List<MonitorSummary> list) {
        monitorSummaryService.updateHandleSummaryByList(list);
    }


    public List<MonitorItem> listMonitorItemByNameList(List<String> itemNameList) {
        return monitorItemService.listMonitorItemByItemName(itemNameList);
    }

    /**
     * 通过标识查找项目名
     *
     * @param projectId
     * @return
     */
    public String getMonitorProjectNameById(Integer projectId) {
        return monitorProjectService.getMonitorProjectByProjectId(projectId).getProjectName();
    }

    /**
     * MonitorConverBean 转换MonitorDetail
     *
     * @param monitorName 监控名
     * @param convertBean 监控抓取bean 转换为MonitorConverBean后的实体
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void convertBeanHandle(String monitorName, MonitorConvertBean convertBean) {
        MonitorItem item = new MonitorItem();
        item.setProjectName(getMonitorProjectNameById(convertBean.getProjectId()));
        item.setProjectId(convertBean.getProjectId());
        Integer itemId = null;
        MonitorItem monitorItem = monitorItemService.getMonitorItemByName(monitorName,convertBean.getProjectId());
        if (monitorItem == null) {
            item.setMonitorItemName(monitorName);
            monitorItemService.saveMonitorItem(item);
            itemId = item.getId();
        } else {
            itemId = monitorItem.getId();
        }
        saveMonitorDetail(convertBean, itemId);
    }

    /**
     * 更新或者插入 MonitorDetail
     *
     * @param monitorDetail
     * @param convertBean
     * @param itemId
     */
    private void saveMonitorDetail(MonitorConvertBean convertBean, Integer itemId) {
        MonitorDetail detail = new MonitorDetail();
        detail.setRequestTime(convertBean.getRequestTotalTime().get());
        detail.setRequestCount(convertBean.getRequestCount().get());
        detail.setMonitorItemId(itemId);
        detail.setAvgTime(convertBean.getRequestTotalTime().get() / convertBean.getRequestCount().get());
        detail.setIp(convertBean.getHost());
        detail.setRequestType(convertBean.getRequestType());
        detail.setUpdateTime(convertBean.getDate());
        monitorDetailService.saveMonitorDetail(detail);
    }

    /**
     * 批量保存
     *
     * @param list
     */
    public void saveBatchList(List<MonitorSummary> list) {
        Integer batchInsert = monitorSummaryService.saveBatchList(list);
        if (batchInsert < 0) {
            logger.error("summary批量保存失败");
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }

    public List<MonitorProjectUrl> listAllUrl() {
        return monitorProjectUrlService.listUrl();
    }

}
