package net.begincode.maiev.handler;

import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.MonitorProjectUrl;
import net.begincode.maiev.service.MonitorProjectUrlService;
import net.begincode.maiev.util.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/4/8  16:09.
 */
@Component
public class UrlHandler {

    private static Logger logger = LoggerFactory.getLogger(UrlHandler.class);

    @Resource
    private MonitorProjectUrlService monitorProjectUrlService;

    public List<MonitorProjectUrl> listMonitorProjectUrlByProjectId(Integer projectId) {
        List<MonitorProjectUrl> list = monitorProjectUrlService.listMonitorProjectUrlByProjectId(projectId);
        if (ListUtils.isNull(list)) {
            throw new BizException(CommonResponseEnum.DATA_NULL);
        }
        return list;
    }

    public void removeMonitorProjectUrlByName(String urlName,Integer projectId) {
        Integer removeFlag = monitorProjectUrlService.removeProjectUrlByName(urlName,projectId);
        if(removeFlag < 0){
            logger.error("移除url失败");
            throw new BizException(CommonResponseEnum.EXCEPTION);
        }
    }


}
