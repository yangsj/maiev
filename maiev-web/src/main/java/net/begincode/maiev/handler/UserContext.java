package net.begincode.maiev.handler;

import net.begincode.maiev.common.BeginCodeConstant;
import net.begincode.maiev.common.BizException;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.MaievUser;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Stay on 2017/5/25  22:05.
 */
@Component
public class UserContext {
    @Resource
    private UserStrategy userStrategy;

    public void login(MaievUser user) {
        userStrategy.login(user);
    }

    public void loginOut() {
        userStrategy.loginOut();
    }

    public void regist(MaievUser maievUser) {
        userStrategy.regist(maievUser);
    }

    public MaievUser getCurrentUser() {
        return userStrategy.getCurrentUser();
    }

    public void checkUserName(String userName) {
        userStrategy.checkUserName(userName);
    }

    public void checkNickName(String nickName) {
        userStrategy.checkNickName(nickName);
    }

    public void checkEmail(String email) {
        userStrategy.checkEmail(email);
    }

    public void checkCode(String code) {
        userStrategy.checkCode(code);
    }

    public MaievUser getUserById(Integer id){
        return userStrategy.getUserById(id);
    }

}
