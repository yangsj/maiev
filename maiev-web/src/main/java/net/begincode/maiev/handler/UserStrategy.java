package net.begincode.maiev.handler;

import net.begincode.maiev.model.MaievUser;

/**
 * 用户接口
 * Created by Stay on 2017/5/25  22:01.
 */
public interface UserStrategy {

    void login(MaievUser user);

    void loginOut();

    void regist(MaievUser user);

    MaievUser getCurrentUser();

    void checkUserName(String userName);

    void checkNickName(String nickName);

    void checkEmail(String email);

    void checkCode(String code);

    MaievUser getUserById(Integer userId);

}
