package net.begincode.maiev.mapper;

import net.begincode.maiev.model.AlarmGroupSelect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/5/14  15:28.
 */
public interface AlarmGroupSelectMapper {

    List<AlarmGroupSelect> listAlarmGroupSelectByProjectId(@Param("projectId") Integer projectId);

}
