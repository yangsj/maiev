package net.begincode.maiev.mapper;

import net.begincode.maiev.model.AlarmItemSelect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/5/14  15:33.
 */
public interface AlarmItemSelectMapper {

    List<AlarmItemSelect> listAlarmItemSelectByProjectId(@Param("projectId") Integer projectId);

}
