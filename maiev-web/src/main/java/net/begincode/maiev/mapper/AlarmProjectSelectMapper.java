package net.begincode.maiev.mapper;

import net.begincode.maiev.model.AlarmProjectSelect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/4/24  21:28.
 */
public interface AlarmProjectSelectMapper {

    List<AlarmProjectSelect> listAlarmProjectSelectByUserId(@Param("userId") Integer userId);

}
