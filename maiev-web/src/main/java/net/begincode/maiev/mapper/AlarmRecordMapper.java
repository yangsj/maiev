package net.begincode.maiev.mapper;

import java.util.List;
import net.begincode.maiev.model.AlarmRecord;
import net.begincode.maiev.model.AlarmRecordExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AlarmRecordMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int countByExample(AlarmRecordExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int deleteByExample(AlarmRecordExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int insert(AlarmRecord record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int insertSelective(AlarmRecord record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    List<AlarmRecord> selectByExampleWithRowbounds(AlarmRecordExample example, RowBounds rowBounds);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    List<AlarmRecord> selectByExample(AlarmRecordExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    AlarmRecord selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") AlarmRecord record, @Param("example") AlarmRecordExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") AlarmRecord record, @Param("example") AlarmRecordExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(AlarmRecord record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table alarm_record
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(AlarmRecord record);
}