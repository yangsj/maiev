package net.begincode.maiev.mapper;

import net.begincode.maiev.model.BizFrontMonitorData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/3/27  19:48.
 */
public interface BizFrontMonitorDataMapper {

    List<BizFrontMonitorData> listFrontMonitorDataByProjectIdWithPage(@Param("projectId") Integer projectId, @Param("currentNum") Integer currentNum,@Param("pageSize") Integer pageSize);

    Integer countFrontMonitorDataByProjectId(@Param("projectId") Integer projectId);


}
