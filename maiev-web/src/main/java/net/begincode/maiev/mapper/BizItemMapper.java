package net.begincode.maiev.mapper;

import net.begincode.maiev.model.BizItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/3/29  14:26.
 */
public interface BizItemMapper {

    List<BizItem> listUrlByGroupId(@Param("groupId") Integer groupId);

    List<BizItem> listExceptItemByProGroId(@Param("projectId") Integer projectId, @Param("groupId") Integer groupId, @Param("current") Integer current, @Param("pageSize") Integer pageSize);

}
