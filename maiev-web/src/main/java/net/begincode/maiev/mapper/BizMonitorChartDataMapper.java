package net.begincode.maiev.mapper;

import net.begincode.maiev.model.BizMonitorChartData;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by Stay on 2017/3/28  16:57.
 */
public interface BizMonitorChartDataMapper {

    List<BizMonitorChartData> listChartDataByDate(@Param("monitorName") String monitorName, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

}
