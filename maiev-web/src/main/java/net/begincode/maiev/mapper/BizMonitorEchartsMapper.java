package net.begincode.maiev.mapper;

import net.begincode.maiev.model.MonitorEcharts;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by Stay on 2017/6/12  14:45.
 */
public interface BizMonitorEchartsMapper {

    List<MonitorEcharts> listEchartsDataByProjectId(@Param("projectId") Integer projectId,
                                                    @Param("beginTime")Date beginTime,@Param("endTime")Date endTime);

}
