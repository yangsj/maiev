package net.begincode.maiev.mapper;

import net.begincode.maiev.model.BizMonitorGroupEchart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/3/29  18:35.
 */
public interface BizMonitorGroupEchartMapper {

    List<BizMonitorGroupEchart> listByGroupId(@Param("groupId") Integer groupId);

    List<BizMonitorGroupEchart> listInGroupId(@Param("list") List<Integer> list);
}
