package net.begincode.maiev.mapper;

import net.begincode.maiev.model.MonitorGroupItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/4/6  16:19.
 */
public interface BizMonitorGroupItemMapper {

    Integer insertBatchByListMonitorGroupItem(@Param("list") List<MonitorGroupItem> list);

}
