package net.begincode.maiev.mapper;

import net.begincode.maiev.model.BizMonitorGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/3/29  12:18.
 */
public interface BizMonitorGroupMapper {

    List<BizMonitorGroup> listGroupByProjectId(@Param("projectId") Integer projectId, @Param("current") Integer current, @Param("pageSize") Integer pageSize);

    Integer countGroupByProject(@Param("projectId") Integer projectId);
}
