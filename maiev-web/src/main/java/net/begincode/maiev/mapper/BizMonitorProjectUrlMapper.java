package net.begincode.maiev.mapper;

import net.begincode.maiev.model.MonitorProjectUrl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/4/6  22:53.
 */
public interface BizMonitorProjectUrlMapper {

    Integer insertBatchByMonitorProjectUrlList(@Param("list") List<MonitorProjectUrl> list);

}
