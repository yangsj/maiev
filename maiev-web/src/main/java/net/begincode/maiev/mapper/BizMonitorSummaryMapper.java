package net.begincode.maiev.mapper;

import net.begincode.maiev.model.MonitorSummary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/2/28  16:41.
 */
public interface BizMonitorSummaryMapper {
    /**
     * 批量插入MonitorSummary
     *
     * @param list
     * @return
     */
    int insertBatchListMonitorSummary(@Param("list") List<MonitorSummary> list);

    /**
     *  根据id号 批量更新handle_flag 0 变成 1
     *
     * @param list
     * @return
     */
    int updateBatchHandleByIdList(@Param("list") List<Integer> list);

    /**
     *  根据集合 批量更新handle_flag 0 变成 1
     *
     * @param list
     * @return
     */
    int updateBatchHandleByList(@Param("list") List<MonitorSummary> list);


}
