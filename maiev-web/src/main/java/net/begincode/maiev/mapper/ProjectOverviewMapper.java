package net.begincode.maiev.mapper;

import net.begincode.maiev.model.ProjectOverview;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Stay on 2017/3/10  12:35.
 */
public interface ProjectOverviewMapper {

    List<ProjectOverview> listByPage(@Param("current") Integer current, @Param("pageSize") Integer pageSize);

}
