package net.begincode.maiev.model;

/**
 * Created by Stay on 2017/5/23  17:26.
 */
public class AlarmFront {
    private Integer id;

    private Integer timeThreshold;

    private Integer qpsThreshold;

    private String alarmName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTimeThreshold() {
        return timeThreshold;
    }

    public void setTimeThreshold(Integer timeThreshold) {
        this.timeThreshold = timeThreshold;
    }

    public Integer getQpsThreshold() {
        return qpsThreshold;
    }

    public void setQpsThreshold(Integer qpsThreshold) {
        this.qpsThreshold = qpsThreshold;
    }

    public String getAlarmName() {
        return alarmName;
    }

    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
    }
}
