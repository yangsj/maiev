package net.begincode.maiev.model;

/**
 * 告警分组选择
 * Created by Stay on 2017/5/14  14:42.
 */
public class AlarmGroupSelect {

    private Integer id;

    private String groupName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
