package net.begincode.maiev.model;

/**
 * Created by Stay on 2017/5/14  14:43.
 */
public class AlarmItemSelect {

    private Integer id;

    private String itemName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
