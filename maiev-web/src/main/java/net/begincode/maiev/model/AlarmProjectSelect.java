package net.begincode.maiev.model;

/**
 * Created by Stay on 2017/4/25  10:52.
 */
public class AlarmProjectSelect {

    private Integer id;

    private String projectName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
