package net.begincode.maiev.model;

import java.io.Serializable;

public class AlarmSet implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column alarm_set.id
     *
     * @mbggenerated
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column alarm_set.time_threshold
     *
     * @mbggenerated
     */
    private Integer timeThreshold;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column alarm_set.qps_threshold
     *
     * @mbggenerated
     */
    private Integer qpsThreshold;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column alarm_set.monitor_item_id
     *
     * @mbggenerated
     */
    private Integer monitorItemId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column alarm_set.group_id
     *
     * @mbggenerated
     */
    private Integer groupId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column alarm_set.user_id
     *
     * @mbggenerated
     */
    private Integer userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column alarm_set.delete_flag
     *
     * @mbggenerated
     */
    private Integer deleteFlag;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table alarm_set
     *
     * @mbggenerated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column alarm_set.id
     *
     * @return the value of alarm_set.id
     *
     * @mbggenerated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column alarm_set.id
     *
     * @param id the value for alarm_set.id
     *
     * @mbggenerated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column alarm_set.time_threshold
     *
     * @return the value of alarm_set.time_threshold
     *
     * @mbggenerated
     */
    public Integer getTimeThreshold() {
        return timeThreshold;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column alarm_set.time_threshold
     *
     * @param timeThreshold the value for alarm_set.time_threshold
     *
     * @mbggenerated
     */
    public void setTimeThreshold(Integer timeThreshold) {
        this.timeThreshold = timeThreshold;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column alarm_set.qps_threshold
     *
     * @return the value of alarm_set.qps_threshold
     *
     * @mbggenerated
     */
    public Integer getQpsThreshold() {
        return qpsThreshold;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column alarm_set.qps_threshold
     *
     * @param qpsThreshold the value for alarm_set.qps_threshold
     *
     * @mbggenerated
     */
    public void setQpsThreshold(Integer qpsThreshold) {
        this.qpsThreshold = qpsThreshold;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column alarm_set.monitor_item_id
     *
     * @return the value of alarm_set.monitor_item_id
     *
     * @mbggenerated
     */
    public Integer getMonitorItemId() {
        return monitorItemId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column alarm_set.monitor_item_id
     *
     * @param monitorItemId the value for alarm_set.monitor_item_id
     *
     * @mbggenerated
     */
    public void setMonitorItemId(Integer monitorItemId) {
        this.monitorItemId = monitorItemId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column alarm_set.group_id
     *
     * @return the value of alarm_set.group_id
     *
     * @mbggenerated
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column alarm_set.group_id
     *
     * @param groupId the value for alarm_set.group_id
     *
     * @mbggenerated
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column alarm_set.user_id
     *
     * @return the value of alarm_set.user_id
     *
     * @mbggenerated
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column alarm_set.user_id
     *
     * @param userId the value for alarm_set.user_id
     *
     * @mbggenerated
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column alarm_set.delete_flag
     *
     * @return the value of alarm_set.delete_flag
     *
     * @mbggenerated
     */
    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column alarm_set.delete_flag
     *
     * @param deleteFlag the value for alarm_set.delete_flag
     *
     * @mbggenerated
     */
    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}