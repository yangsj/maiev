package net.begincode.maiev.model;

import java.util.Date;

/**
 * 监控项目主页table数据
 * Created by Stay on 2017/3/27  19:02.
 */
public class BizFrontMonitorData {

    private String monitorName;

    private String ip;

    private String type;

    private Integer requestCount;

    private Long requestTotalTime;

    private Long avgTime;

    private Date updateTime;

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Integer requestCount) {
        this.requestCount = requestCount;
    }

    public Long getRequestTotalTime() {
        return requestTotalTime;
    }

    public void setRequestTotalTime(Long requestTotalTime) {
        this.requestTotalTime = requestTotalTime;
    }

    public Long getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(Long avgTime) {
        this.avgTime = avgTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
