package net.begincode.maiev.model;

/**
 * Created by Stay on 2017/3/29  14:24.
 */
public class BizItem {

    private Integer itemId;

    private String monitorUrlName;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getMonitorUrlName() {
        return monitorUrlName;
    }

    public void setMonitorUrlName(String monitorUrlName) {
        this.monitorUrlName = monitorUrlName;
    }
}
