package net.begincode.maiev.model;

import java.util.Date;

/**
 * 项目主页url点击显示图表数据
 * Created by Stay on 2017/3/28  16:53.
 */
public class BizMonitorChartData {

    private String monitorName;

    private String maxRequestTime;

    private Date createTime;

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public String getMaxRequestTime() {
        return maxRequestTime;
    }

    public void setMaxRequestTime(String maxRequestTime) {
        this.maxRequestTime = maxRequestTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
