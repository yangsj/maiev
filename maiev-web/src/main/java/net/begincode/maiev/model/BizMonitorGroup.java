package net.begincode.maiev.model;

import java.util.Date;

/**
 * Created by Stay on 2017/3/28  20:20.
 */
public class BizMonitorGroup {

    private Integer id;

    private String groupName;

    private Date updateTime;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
