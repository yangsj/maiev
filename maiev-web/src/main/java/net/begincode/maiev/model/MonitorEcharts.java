package net.begincode.maiev.model;

import java.util.List;

/**
 * Created by Stay on 2017/6/12  14:34.
 */
public class MonitorEcharts {
    private Integer id;

    private String groupName;

    private List<MonitorEchartsItem> list;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<MonitorEchartsItem> getList() {
        return list;
    }

    public void setList(List<MonitorEchartsItem> list) {
        this.list = list;
    }
}
