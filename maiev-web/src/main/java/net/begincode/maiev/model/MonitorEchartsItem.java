package net.begincode.maiev.model;

import java.util.List;

/**
 * Created by Stay on 2017/6/12  14:34.
 */
public class MonitorEchartsItem {

    private Integer monitorItemId;

    private String monitorItemName;

    private List<MonitorEchartsItemData> monitorList;

    public Integer getMonitorItemId() {
        return monitorItemId;
    }

    public void setMonitorItemId(Integer monitorItemId) {
        this.monitorItemId = monitorItemId;
    }

    public String getMonitorItemName() {
        return monitorItemName;
    }

    public void setMonitorItemName(String monitorItemName) {
        this.monitorItemName = monitorItemName;
    }

    public List<MonitorEchartsItemData> getMonitorList() {
        return monitorList;
    }

    public void setMonitorList(List<MonitorEchartsItemData> monitorList) {
        this.monitorList = monitorList;
    }
}
