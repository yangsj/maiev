package net.begincode.maiev.model;

import java.util.Date;

/**
 * Created by Stay on 2017/6/12  16:41.
 */
public class MonitorEchartsItemData {

    private Integer avgTime;

    private Integer requestCount;

    private String requestType;

    private Date updateTime;

    public Integer getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(Integer avgTime) {
        this.avgTime = avgTime;
    }

    public Integer getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Integer requestCount) {
        this.requestCount = requestCount;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
