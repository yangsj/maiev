package net.begincode.maiev.model;

import java.io.Serializable;
import java.util.Date;

public class MonitorProject implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column monitor_project.id
     *
     * @mbggenerated
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column monitor_project.project_name
     *
     * @mbggenerated
     */
    private String projectName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column monitor_project.user_id
     *
     * @mbggenerated
     */
    private Integer userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column monitor_project.create_time
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column monitor_project.project_status
     *
     * @mbggenerated
     */
    private Integer projectStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column monitor_project.note
     *
     * @mbggenerated
     */
    private String note;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column monitor_project.delete_flag
     *
     * @mbggenerated
     */
    private Integer deleteFlag;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table monitor_project
     *
     * @mbggenerated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column monitor_project.id
     *
     * @return the value of monitor_project.id
     *
     * @mbggenerated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column monitor_project.id
     *
     * @param id the value for monitor_project.id
     *
     * @mbggenerated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column monitor_project.project_name
     *
     * @return the value of monitor_project.project_name
     *
     * @mbggenerated
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column monitor_project.project_name
     *
     * @param projectName the value for monitor_project.project_name
     *
     * @mbggenerated
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column monitor_project.user_id
     *
     * @return the value of monitor_project.user_id
     *
     * @mbggenerated
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column monitor_project.user_id
     *
     * @param userId the value for monitor_project.user_id
     *
     * @mbggenerated
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column monitor_project.create_time
     *
     * @return the value of monitor_project.create_time
     *
     * @mbggenerated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column monitor_project.create_time
     *
     * @param createTime the value for monitor_project.create_time
     *
     * @mbggenerated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column monitor_project.project_status
     *
     * @return the value of monitor_project.project_status
     *
     * @mbggenerated
     */
    public Integer getProjectStatus() {
        return projectStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column monitor_project.project_status
     *
     * @param projectStatus the value for monitor_project.project_status
     *
     * @mbggenerated
     */
    public void setProjectStatus(Integer projectStatus) {
        this.projectStatus = projectStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column monitor_project.note
     *
     * @return the value of monitor_project.note
     *
     * @mbggenerated
     */
    public String getNote() {
        return note;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column monitor_project.note
     *
     * @param note the value for monitor_project.note
     *
     * @mbggenerated
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column monitor_project.delete_flag
     *
     * @return the value of monitor_project.delete_flag
     *
     * @mbggenerated
     */
    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column monitor_project.delete_flag
     *
     * @param deleteFlag the value for monitor_project.delete_flag
     *
     * @mbggenerated
     */
    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}