package net.begincode.maiev.model;

/**
 * 前台  项目监控情况
 * Created by Stay on 2017/3/10  11:02.
 */
public class ProjectOverview {

    private String projectName;

    private Integer groupCount;

    private Integer projectStatus;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Integer getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }

    public Integer getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(Integer projectStatus) {
        this.projectStatus = projectStatus;
    }
}
