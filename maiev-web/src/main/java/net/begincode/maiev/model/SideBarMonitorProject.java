package net.begincode.maiev.model;

/**
 * Created by Stay on 2017/3/6  15:53.
 */
public class SideBarMonitorProject {

    private Integer projectId;

    private String projectName;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
