package net.begincode.maiev.monitor;

import net.begincode.maiev.bean.MonitorConvertBean;
import net.begincode.maiev.handler.SummaryTaskHandler;
import net.begincode.maiev.model.MonitorSummary;
import net.begincode.maiev.monitor.bean.CollectContainer;
import net.begincode.maiev.util.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Stay on 2017/3/13  17:26.
 */
@Component
public class HandleSummary {

    private Logger logger = LoggerFactory.getLogger(HandleSummary.class);

    @Resource
    private SummaryTaskHandler summaryTaskHandler;

    private CollectContainer collectContainer = CollectContainer.getInstance();   //收集一分钟抓取的数据汇总容器


    /**
     * 处理summary未处理的数据
     */
    public void summaryHandle() {
        List<MonitorSummary> noHandleList = summaryTaskHandler.listMinuteAgoNoHandleSumary();
        if (!ListUtils.isNull(noHandleList)) {
            MonitorSummary summary = null;
            for (int i = 0; i < noHandleList.size(); i++) {
                summary = noHandleList.get(i);
                collectContainer.recordMonitorData(summary.getMonitorName(), summary.getTotalRequestTime(), summary.getRequestType(), summary.getMonitorHost(), summary.getRequestCount(), summary.getProjectId(), summary.getMaxRequestTime(),summary.getCreateDate());
            }
            summaryTaskHandler.updateHandleFlagSummary(noHandleList);
        }
    }


    /**
     * 处理容器中的数据
     */
    public void mapLoop() {
        HashMap<String, MonitorConvertBean> map = new HashMap();
        collectContainer.getDataMap(map);
        for (Map.Entry<String, MonitorConvertBean> entry : map.entrySet()) {
            summaryTaskHandler.convertBeanHandle(entry.getKey(), entry.getValue());
        }
    }


}
