package net.begincode.maiev.monitor;

import net.begincode.maiev.handler.AlarmTaskHandler;
import net.begincode.maiev.model.AlarmSet;
import net.begincode.maiev.monitor.mail.AlarmSetObserver;
import net.begincode.maiev.monitor.mail.SubjectOperation;
import net.begincode.maiev.util.ListUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.annotation.Resource;
import java.util.List;

/**
 * spring初始化完成后调用
 * Created by Stay on 2017/1/12  0:08.
 */
public class TracingBeanPostProcessor implements ApplicationListener<ContextRefreshedEvent> {

    @Resource
    private AlarmTaskHandler alarmTaskHandler;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<AlarmSet> alarmSets = alarmTaskHandler.listAllAlarmSet();
        AlarmSetObserver alarmSetObserver = null;
        if (!ListUtils.isNull(alarmSets)) {
            SubjectOperation instance = SubjectOperation.getInstance();
            for (int i = 0; i < alarmSets.size(); i++) {
                alarmSetObserver = new AlarmSetObserver(alarmSets.get(i));
                instance.addObserver(alarmSetObserver);
            }
        }
    }
}
