package net.begincode.maiev.monitor.data;

/**
 * Created by Stay on 2017/6/14  11:29.
 */
public interface DataOperation {

    void handle(Object data);

}
