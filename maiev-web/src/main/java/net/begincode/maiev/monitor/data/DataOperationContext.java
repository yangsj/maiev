package net.begincode.maiev.monitor.data;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * 收集数据处理主入口
 * Created by Stay on 2017/6/14  11:30.
 */
@Component
public class DataOperationContext {

    @Resource(name = "dataOperationHashMap")
    HashMap<String, DataOperation> map;


    public void handle(String type, Object data) {
        DataOperation operation = map.get(type);
        if (operation != null) {
            operation.handle(data);
        }
    }

}
