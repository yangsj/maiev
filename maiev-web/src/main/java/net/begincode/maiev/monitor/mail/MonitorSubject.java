package net.begincode.maiev.monitor.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Stay on 2017/6/1  10:53.
 */
public class MonitorSubject implements Subject {

    List<Observer> list = new ArrayList<>();
    private ReentrantLock reentrantLock = new ReentrantLock();

    @Override
    public void addData(Observer observer) {
        reentrantLock.lock();
        try {
            list.add(observer);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override
    public void removeData(Observer observer) {
        reentrantLock.lock();
        try {
            list.remove(observer);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : list) {
            observer.alertInspect();
        }
    }

    public void resetList(List<Observer> observerList){
         list = observerList;
    }


}
