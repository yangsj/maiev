package net.begincode.maiev.monitor.mail;

/**
 * 监控主题 添加告警观察者
 * 每次有数据添加进来就记录 并且观察有没有告警
 * Created by Stay on 2017/5/31  22:14.
 */
public interface Subject {

    void addData(Observer observer);

    void removeData(Observer observer);

    void notifyObservers();

}
