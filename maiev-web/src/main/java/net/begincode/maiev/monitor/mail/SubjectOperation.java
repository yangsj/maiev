package net.begincode.maiev.monitor.mail;

import java.util.List;

/**
 * Created by Stay on 2017/6/4  13:47.
 */
public class SubjectOperation {

    private MonitorSubject monitorSubject = new MonitorSubject();

    private static enum Singleton {
        INSTANCE;
        private SubjectOperation singleton;

        private Singleton() {
            singleton = new SubjectOperation();
        }

        public SubjectOperation getSingleton() {
            return singleton;
        }
    }

    public static SubjectOperation getInstance(){
        return Singleton.INSTANCE.getSingleton();
    }


    public void addObserver(Observer observer){
        monitorSubject.addData(observer);
    }

    public void notifyAllObservers(){
        monitorSubject.notifyObservers();
    }

    public void resetList(List<Observer> list){
        monitorSubject.resetList(list);
    }


}
