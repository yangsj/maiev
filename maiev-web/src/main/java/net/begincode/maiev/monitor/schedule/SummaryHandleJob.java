package net.begincode.maiev.monitor.schedule;

import net.begincode.maiev.bean.SummaryBean;
import net.begincode.maiev.handler.AlarmTaskHandler;
import net.begincode.maiev.handler.SummaryTaskHandler;
import net.begincode.maiev.model.AlarmSet;
import net.begincode.maiev.model.MonitorProjectUrl;
import net.begincode.maiev.monitor.HandleSummary;
import net.begincode.maiev.monitor.ThreadPool;
import net.begincode.maiev.monitor.mail.AlarmSetObserver;
import net.begincode.maiev.monitor.mail.Observer;
import net.begincode.maiev.monitor.mail.SubjectOperation;
import net.begincode.maiev.monitor.thread.MonitorDataTask;
import net.begincode.maiev.util.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stay on 2017/5/26  23:04.
 */
@Component
public class SummaryHandleJob {

    private static final Logger logger = LoggerFactory.getLogger(SummaryHandleJob.class);

    @Resource
    private HandleSummary handleSummary;

    private ThreadPool threadPool = ThreadPool.getInstance();
    @Resource
    private SummaryTaskHandler summaryTaskHandler;
    @Resource
    private AlarmTaskHandler alarmTaskHandler;


    @Scheduled(fixedRate = 1000 * 60)  //每隔60秒触发此方法
    public void sendData() {
        List<MonitorProjectUrl> list = summaryTaskHandler.listAllUrl();
        SummaryBean summaryBean = null;
        logger.info("监控的url个数为:" + list.size());
        MonitorProjectUrl monitorProjectUrl = null;
        //遍历数据库取出的url
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                monitorProjectUrl = list.get(i);
                summaryBean = new SummaryBean();
                summaryBean.setProjectId(monitorProjectUrl.getProjectId());
                //创建任务
                MonitorDataTask monitorDataTask = new MonitorDataTask(monitorProjectUrl.getMonitorUrl(), summaryBean);
                //执行任务
                threadPool.addTask(monitorDataTask);
            }
        }
    }

    /**
     * 处理summary数据进入container容器
     *
     */
    @Scheduled(fixedRate = 1000 * 30)  //每隔30秒触发此方法
    public void handle() {
        handleSummary.summaryHandle();     //处理收集到的数据到container容器中
    }

    /**
     * 告警处理
     * 并汇总进detail表中
     *
     */
    @Scheduled(fixedRate = 1000 * 40)  //每隔40秒触发此方法
    public void handleContainer() {
        SubjectOperation.getInstance().notifyAllObservers();    //通知所有告警设置 比较告警
        handleSummary.mapLoop();    //处理container容器中的数据进入detail表中 并清空容器中的数据
    }

    /**
     * 重新写入告警
     *
     */
    @Scheduled(fixedRate = 1000 * 300)
    public void resetAlarmEmail() {
        List<AlarmSet> alarmSets = alarmTaskHandler.listAllAlarmSet();
        AlarmSetObserver alarmSetObserver = null;
        if(!ListUtils.isNull(alarmSets)) {
            SubjectOperation instance = SubjectOperation.getInstance();
            List<Observer> list = new ArrayList<>(alarmSets.size());
            for (int i = 0; i < alarmSets.size(); i++) {
                alarmSetObserver = new AlarmSetObserver(alarmSets.get(i));
                list.add(alarmSetObserver);
            }
            instance.resetList(list);
        }
    }


}
