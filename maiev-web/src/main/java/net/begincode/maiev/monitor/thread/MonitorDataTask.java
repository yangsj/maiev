package net.begincode.maiev.monitor.thread;

import com.fasterxml.jackson.core.type.TypeReference;
import net.begincode.maiev.bean.MonitorBean;
import net.begincode.maiev.bean.SummaryBean;
import net.begincode.maiev.monitor.data.DataOperationContext;
import net.begincode.maiev.monitor.http.HttpGrab;
import net.begincode.maiev.util.SpringApplicationContextHolder;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * Created by Stay on 2017/3/18  14:20.
 */
public class MonitorDataTask implements Runnable {
    private String url;
    private SummaryBean summaryBean;


    public MonitorDataTask(String url, SummaryBean summaryBean) {
        this.url = url;
        this.summaryBean = summaryBean;
    }

    @Override
    public void run() {
        List list = HttpGrab.send(summaryBean.getProjectId(), url, new TypeReference<List<MonitorBean>>() {
        });
        DataOperationContext dataOperationContext = (DataOperationContext) SpringApplicationContextHolder.getSpringBean("dataOperationContext");
        if (!CollectionUtils.isEmpty(list)) {
            list.add(summaryBean.getProjectId());
            dataOperationContext.handle("HTTP", list);
        }
    }


}
