package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.AlarmSet;

/**
 * Created by Stay on 2017/4/25  13:54.
 */
public class AlarmSaveParam extends Param {
    private AlarmSet alarmSet;


    @Override
    public void check() {
        checkArgs((alarmSet.getGroupId() == null && alarmSet.getMonitorItemId() == null), CommonResponseEnum.PARAM_ERROR);
        checkQPSAndTime(alarmSet.getQpsThreshold(), alarmSet.getTimeThreshold());
    }

    protected void checkQPSAndTime(Integer qpsThreshold, Integer timeThreshold) {
        checkArgs(qpsThreshold > 0 && qpsThreshold != null, CommonResponseEnum.PARAM_ERROR);
        checkArgs(timeThreshold > 0 && timeThreshold != null, CommonResponseEnum.PARAM_ERROR);
    }

    public AlarmSet getAlarmSet() {
        return alarmSet;
    }

    public void setAlarmSet(AlarmSet alarmSet) {
        this.alarmSet = alarmSet;
    }
}
