package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.AlarmSet;

/**
 * Created by Stay on 2017/5/24  16:58.
 */
public class AlarmSetUpdateParam extends Param {

    private AlarmSet alarmSet;

    @Override
    public void check() {
        checkQPSAndTime(alarmSet.getQpsThreshold(), alarmSet.getTimeThreshold());
    }

    protected void checkQPSAndTime(Integer qpsThreshold, Integer timeThreshold) {
        checkNotEmpty(alarmSet.getId().toString(), CommonResponseEnum.PARAM_ERROR);
        checkArgs(qpsThreshold > 0 && qpsThreshold != null, CommonResponseEnum.PARAM_ERROR);
        checkArgs(timeThreshold > 0 && timeThreshold != null, CommonResponseEnum.PARAM_ERROR);
    }


    public AlarmSet getAlarmSet() {
        return alarmSet;
    }

    public void setAlarmSet(AlarmSet alarmSet) {
        this.alarmSet = alarmSet;
    }
}
