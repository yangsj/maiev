package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.MonitorProject;

/**
 * Created by Stay on 2017/4/6  20:27.
 */
public class CreateMonitorProjectParam extends Param {
    private MonitorProject monitorProject;

    private String[] grabUrlNames;


    @Override
    public void check() {
        checkNotEmpty(monitorProject.getProjectName(), CommonResponseEnum.PARAM_ERROR);
        checkNotNull(grabUrlNames, CommonResponseEnum.PARAM_ERROR);
    }


    public MonitorProject getMonitorProject() {
        return monitorProject;
    }

    public void setMonitorProject(MonitorProject monitorProject) {
        this.monitorProject = monitorProject;
    }

    public String[] getGrabUrlNames() {
        return grabUrlNames;
    }

    public void setGrabUrlNames(String[] grabUrlNames) {
        this.grabUrlNames = grabUrlNames;
    }
}
