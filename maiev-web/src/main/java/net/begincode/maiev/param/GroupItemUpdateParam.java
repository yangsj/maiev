package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;

/**
 * Created by Stay on 2017/4/6  16:45.
 */
public class GroupItemUpdateParam extends Param {

    private Integer groupId;

    private int[] itemIds;

    @Override
    public void check() {
        checkNotEmpty(groupId.toString(), CommonResponseEnum.PARAM_ERROR);
        checkNotNull(itemIds, CommonResponseEnum.PARAM_ERROR);
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public int[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(int[] itemIds) {
        this.itemIds = itemIds;
    }
}
