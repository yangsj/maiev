package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.MaievUser;

/**
 * Created by Stay on 2017/5/31  16:38.
 */
public class MaievUserCheckParam extends Param {
    private String checkName;

    private MaievUser maievUser;

    private String code;


    @Override
    public void check() {
        checkNotEmpty(checkName, CommonResponseEnum.PARAM_ERROR);
    }

    public String getCheckName() {
        return checkName;
    }

    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    public MaievUser getMaievUser() {
        return maievUser;
    }

    public void setMaievUser(MaievUser maievUser) {
        this.maievUser = maievUser;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
