package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.common.BeginCodeConstant;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.enums.ResponseEnum;
import net.begincode.maiev.enums.VerCodeResponseEnum;
import net.begincode.maiev.model.MaievUser;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

public class MaievUserRegParam extends Param {
    private MaievUser maievUser;
    private String code;

    @Override
    public void check() {
        checkNotEmpty(maievUser.getUsername(), CommonResponseEnum.PARAM_ERROR);
        checkNotEmpty(maievUser.getNickname(), CommonResponseEnum.PARAM_ERROR);
        checkNotEmpty(maievUser.getEmail(), CommonResponseEnum.PARAM_ERROR);
        checkNotEmpty(maievUser.getPwd(), CommonResponseEnum.PARAM_ERROR);
        checkVerCode(code, VerCodeResponseEnum.VERCODE_VALIDATE_ERROR);
    }

    public MaievUser getMaievUser() {
        return maievUser;
    }

    public void setMaievUser(MaievUser maievUser) {
        this.maievUser = maievUser;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    private void checkVerCode(String code, ResponseEnum responseEnum){
        HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
        checkArgs(code.toLowerCase().equals(session.getAttribute(BeginCodeConstant.VERCODE).toString()),responseEnum);
    }

}
