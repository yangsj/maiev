package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;

/**
 * Created by Stay on 2017/3/31  23:00.
 */
public class MonitorGroupDeleteParam extends Param {

    private Integer id;

    @Override
    public void check() {
        checkNotEmpty(id.toString(), CommonResponseEnum.PARAM_ERROR);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
