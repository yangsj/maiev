package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;

/**
 * Created by Stay on 2017/3/30  19:56.
 */
public class MonitorGroupNameEditParam extends Param {

    private Integer id;

    private String groupName;

    @Override
    public void check() {
        checkNotEmpty(id.toString(), CommonResponseEnum.PARAM_ERROR);
        checkNotEmpty(groupName, CommonResponseEnum.PARAM_ERROR);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
