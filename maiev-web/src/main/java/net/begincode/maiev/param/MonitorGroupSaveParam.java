package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;
import net.begincode.maiev.model.MonitorGroup;

/**
 * Created by Stay on 2017/3/30  14:59.
 */
public class MonitorGroupSaveParam extends Param {

    private MonitorGroup monitorGroup;
    private int[] itemIdArray;


    @Override
    public void check() {
        checkNotEmpty(monitorGroup.getGroupName(), CommonResponseEnum.PARAM_ERROR);
        checkNotNull(itemIdArray, CommonResponseEnum.PARAM_ERROR);
    }

    public MonitorGroup getMonitorGroup() {
        return monitorGroup;
    }

    public int[] getItemIdArray() {
        return itemIdArray;
    }

    public void setMonitorGroup(MonitorGroup monitorGroup) {
        this.monitorGroup = monitorGroup;
    }

    public void setItemIdArray(int[] itemIdArray) {
        this.itemIdArray = itemIdArray;
    }
}
