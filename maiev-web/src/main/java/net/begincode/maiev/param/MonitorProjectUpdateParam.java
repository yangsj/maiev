package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;

/**
 * Created by Stay on 2017/4/7  18:41.
 */
public class MonitorProjectUpdateParam extends Param {
    private String projectName;

    private String note;

    private String[] urls;

    @Override
    public void check() {
        checkNotEmpty(projectName, CommonResponseEnum.PARAM_ERROR);
        checkNotNull(urls, CommonResponseEnum.PARAM_ERROR);
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String[] getUrls() {
        return urls;
    }

    public void setUrls(String[] urls) {
        this.urls = urls;
    }
}
