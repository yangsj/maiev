package net.begincode.maiev.param;

import net.begincode.maiev.bean.Param;
import net.begincode.maiev.enums.CommonResponseEnum;

/**
 * Created by Stay on 2017/4/12  13:25.
 */
public class MonitorUrlDeleteParam extends Param {

    private String urlName;

    private Integer projectId;

    @Override
    public void check() {
        checkNotEmpty(urlName, CommonResponseEnum.PARAM_ERROR);
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
}
