package net.begincode.maiev.service;

import net.begincode.maiev.mapper.AlarmGroupSelectMapper;
import net.begincode.maiev.mapper.AlarmItemSelectMapper;
import net.begincode.maiev.mapper.AlarmProjectSelectMapper;
import net.begincode.maiev.model.AlarmGroupSelect;
import net.begincode.maiev.model.AlarmItemSelect;
import net.begincode.maiev.model.AlarmProjectSelect;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/4/25  11:13.
 */
@Service
public class AlarmProjectSelectService {
    @Resource
    private AlarmProjectSelectMapper alarmProjectSelectMapper;
    @Resource
    private AlarmGroupSelectMapper alarmGroupSelectMapper;
    @Resource
    private AlarmItemSelectMapper alarmItemSelectMapper;

    /**
     * 告警设置项目选择显示 list
     *
     * @param userId
     * @return
     */
    public List<AlarmProjectSelect> listAlarmProjectSelectByUserId(Integer userId) {
        return alarmProjectSelectMapper.listAlarmProjectSelectByUserId(userId);
    }

    /**
     * 告警设置分组选择显示
     *
     * @param projectId
     * @return
     */
    public List<AlarmGroupSelect> listGroupSelectByProjectId(Integer projectId) {
        return alarmGroupSelectMapper.listAlarmGroupSelectByProjectId(projectId);
    }

    /**
     * 告警item前台显示
     *
     * @param projectId
     * @return
     */
    public List<AlarmItemSelect> listItemSelectByProjectId(Integer projectId) {
        return alarmItemSelectMapper.listAlarmItemSelectByProjectId(projectId);
    }


}
