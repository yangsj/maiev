package net.begincode.maiev.service;

import net.begincode.maiev.enums.ReadFlagEnum;
import net.begincode.maiev.mapper.AlarmRecordMapper;
import net.begincode.maiev.model.AlarmRecord;
import net.begincode.maiev.model.AlarmRecordExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/5/25  16:12.
 */
@Service
public class AlarmRecordService {
    @Resource
    private AlarmRecordMapper alarmRecordMapper;

    public Integer countAlarmRecordByUserId(Integer userId) {
        AlarmRecordExample alarmRecordExample = new AlarmRecordExample();
        alarmRecordExample.createCriteria().andUserIdEqualTo(userId).andReadFlagEqualTo(ReadFlagEnum.NO_READ.getCode());
        return alarmRecordMapper.countByExample(alarmRecordExample);
    }

    public List<AlarmRecord> listAlarmRecordByUserId(Integer userId) {
        AlarmRecordExample alarmRecordExample = new AlarmRecordExample();
        alarmRecordExample.createCriteria().andUserIdEqualTo(userId);
        return alarmRecordMapper.selectByExample(alarmRecordExample);
    }

    /**
     * 保存告警信息
     *
     * @param alarmRecord
     */
    public void saveAlarmRecord(AlarmRecord alarmRecord) {
        alarmRecordMapper.insertSelective(alarmRecord);
    }


}
