package net.begincode.maiev.service;

import net.begincode.maiev.enums.DelFlagEnum;
import net.begincode.maiev.mapper.AlarmSetMapper;
import net.begincode.maiev.model.AlarmSet;
import net.begincode.maiev.model.AlarmSetExample;
import net.begincode.maiev.util.ListUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class AlarmSetService {
    @Resource
    private AlarmSetMapper alarmSetMapper;

    /**
     * 分页显示 告警设置(alarmSet)
     *
     * @param userId
     * @param currentNum
     * @param eachSize
     * @return
     */
    public List<AlarmSet> listAlarmByUserIdWithRowbounds(Integer userId, Integer currentNum, Integer eachSize) {
        AlarmSetExample alarmSetExample = new AlarmSetExample();
        alarmSetExample.createCriteria()
                .andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode())
                .andUserIdEqualTo(userId);
        alarmSetExample.setOrderByClause("id desc");
        return alarmSetMapper.selectByExampleWithRowbounds(alarmSetExample, new RowBounds((currentNum - 1) * eachSize, eachSize));
    }

    /**
     * 保存alarmSet
     *
     * @param alarmSet
     */
    public void saveAlarmSet(AlarmSet alarmSet) {
        alarmSetMapper.insertSelective(alarmSet);
    }

    /**
     *  根据userId计算AlarmSet数量
     *
     * @param userId
     * @return
     */
    public Integer countAlarmSetByUserId(Integer userId) {
        AlarmSetExample alarmSetExample = new AlarmSetExample();
        alarmSetExample.createCriteria().andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode()).andUserIdEqualTo(userId);
        return alarmSetMapper.countByExample(alarmSetExample);
    }

    /**
     * 修改
     *
     * @param alarmSet
     * @return
     */
    public Integer updateByAlarmSet(AlarmSet alarmSet) {
        return alarmSetMapper.updateByPrimaryKeySelective(alarmSet);
    }

    /**
     * 根据主键和userId查询AlarmSet
     *
     * @param id
     * @param userId
     * @return
     */
    public AlarmSet getAlarmSetById(Integer id, Integer userId) {
        AlarmSetExample alarmSetExample = new AlarmSetExample();
        alarmSetExample.createCriteria().andIdEqualTo(id)
                .andUserIdEqualTo(userId)
                .andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<AlarmSet> alarmSets = alarmSetMapper.selectByExample(alarmSetExample);
        if (CollectionUtils.isEmpty(alarmSets)) {
            return null;
        }
        return alarmSetMapper.selectByExample(alarmSetExample).get(0);
    }

    /**
     * 根据主键和userId删除AlarmSet
     *
     * @param id
     * @param userId
     */
    public void deleteAlarmSetByIdAndUserId(Integer id, Integer userId) {
        AlarmSetExample alarmSetExample = new AlarmSetExample();
        alarmSetExample.createCriteria().andUserIdEqualTo(userId).andIdEqualTo(id).andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<AlarmSet> alarmSets = alarmSetMapper.selectByExample(alarmSetExample);
        AlarmSet alarmSet = alarmSets.get(0);
        alarmSet.setDeleteFlag(DelFlagEnum.IS_DEL.getCode());
        alarmSetMapper.updateByPrimaryKeySelective(alarmSet);
    }

    /**
     * 查询所有告警设置
     *
     * @return
     */
    public List<AlarmSet> listAllAlarmSet() {
        AlarmSetExample alarmSetExample = new AlarmSetExample();
        alarmSetExample.createCriteria().andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        return alarmSetMapper.selectByExample(alarmSetExample);

    }


}
