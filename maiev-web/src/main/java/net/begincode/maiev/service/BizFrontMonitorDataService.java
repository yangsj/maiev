package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizFrontMonitorDataMapper;
import net.begincode.maiev.model.BizFrontMonitorData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/3/27  21:50.
 */
@Service
public class BizFrontMonitorDataService {

    @Resource
    private BizFrontMonitorDataMapper bizFrontMonitorDataMapper;


    /**
     * 分页显示前台监控数据
     *
     * @param projectId
     * @param currentNum
     * @param pageSize
     * @return
     */
    public List<BizFrontMonitorData> listFrontMonitorDataByProjectIdWithPage(Integer projectId, Integer currentNum, Integer pageSize) {
        return bizFrontMonitorDataMapper.listFrontMonitorDataByProjectIdWithPage(projectId, currentNum, pageSize);
    }

    /**
     * 根据项目id 计算数值
     *
     * @param projectId
     * @return
     */
    public Integer countFrontMonitorDataByProjectId(Integer projectId){
        return bizFrontMonitorDataMapper.countFrontMonitorDataByProjectId(projectId);
    }



}
