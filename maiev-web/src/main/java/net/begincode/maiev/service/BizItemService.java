package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizItemMapper;
import net.begincode.maiev.model.BizItem;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/3/29  14:28.
 */
@Service
public class BizItemService {
    @Resource
    private BizItemMapper bizMonitorGroupUrlMapper;

    public List<BizItem> listUrlByGroupId(Integer groupId) {
        return bizMonitorGroupUrlMapper.listUrlByGroupId(groupId);
    }

    public List<BizItem> listNoInGroupItemByProGroId(Integer projectId, Integer groupId, Integer current, Integer pageSize) {
        return bizMonitorGroupUrlMapper.listExceptItemByProGroId(projectId, groupId, current * pageSize, pageSize);
    }


}
