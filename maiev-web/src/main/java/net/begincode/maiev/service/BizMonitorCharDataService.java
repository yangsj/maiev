package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizMonitorChartDataMapper;
import net.begincode.maiev.model.BizMonitorChartData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by Stay on 2017/3/28  17:19.
 */
@Service
public class BizMonitorCharDataService {

    @Resource
    private BizMonitorChartDataMapper bizMonitorChartDataMapper;


    /**
     * item图表显示
     *
     * @param monitorName
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<BizMonitorChartData> listChartDataByDate(String monitorName, Date beginDate, Date endDate) {
        return bizMonitorChartDataMapper.listChartDataByDate(monitorName, beginDate, endDate);
    }


}
