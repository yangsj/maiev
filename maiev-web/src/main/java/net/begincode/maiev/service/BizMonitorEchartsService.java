package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizMonitorEchartsMapper;
import net.begincode.maiev.model.MonitorEcharts;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by Stay on 2017/6/12  15:05.
 */
@Service
public class BizMonitorEchartsService {
    @Resource
    private BizMonitorEchartsMapper bizMonitorEchartsMapper;

    public List<MonitorEcharts> listEchartsDataByProjectId(Integer projectId, Date beginTime, Date endTime) {
        return bizMonitorEchartsMapper.listEchartsDataByProjectId(projectId, beginTime, endTime);
    }

}
