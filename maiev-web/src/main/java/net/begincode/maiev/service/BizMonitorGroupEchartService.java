package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizMonitorGroupEchartMapper;
import net.begincode.maiev.model.BizMonitorGroupEchart;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/3/29  18:55.
 */
@Service
public class BizMonitorGroupEchartService {
    @Resource
    private BizMonitorGroupEchartMapper bizMonitorGroupEchartMapper;

    /**
     * 分组echarts数据封装
     *
     * @param groupId
     * @return
     */
    public List<BizMonitorGroupEchart> listByGroupId(Integer groupId){
        return bizMonitorGroupEchartMapper.listByGroupId(groupId);
    }


}
