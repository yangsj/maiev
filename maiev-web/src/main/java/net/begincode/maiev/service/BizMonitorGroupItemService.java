package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizMonitorGroupItemMapper;
import net.begincode.maiev.model.MonitorGroupItem;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/4/6  16:35.
 */
@Service
public class BizMonitorGroupItemService {
    @Resource
    private BizMonitorGroupItemMapper bizMonitorGroupItemMapper;

    public void insertBatchByListMonitorGroupItem(List<MonitorGroupItem> list) {
        bizMonitorGroupItemMapper.insertBatchByListMonitorGroupItem(list);
    }

}
