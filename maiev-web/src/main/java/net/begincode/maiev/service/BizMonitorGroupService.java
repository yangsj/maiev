package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizMonitorGroupMapper;
import net.begincode.maiev.model.BizMonitorGroup;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/3/29  12:23.
 */
@Service
public class BizMonitorGroupService {
    @Resource
    private BizMonitorGroupMapper bizMonitorGroupMapper;

    /**
     *
     * @param projectId
     * @param currentNum
     * @param pageSize
     * @return
     */
    public List<BizMonitorGroup> listGroupByProjectId(Integer projectId, Integer currentNum, Integer pageSize) {
        return bizMonitorGroupMapper.listGroupByProjectId(projectId, currentNum, pageSize);
    }

    public Integer countGroupByProjectId(Integer projectId) {
        return bizMonitorGroupMapper.countGroupByProject(projectId);
    }


}
