package net.begincode.maiev.service;

import net.begincode.maiev.mapper.BizMonitorProjectUrlMapper;
import net.begincode.maiev.model.MonitorProjectUrl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/4/6  23:11.
 */
@Service
public class BizMonitorProjectUrlService {
    @Resource
    private BizMonitorProjectUrlMapper bizMonitorProjectUrlMapper;

    /**
     * 批量添加
     *
     * @param list
     */
    public void saveBatchByMonitorProjectUrlList(List<MonitorProjectUrl> list){
        bizMonitorProjectUrlMapper.insertBatchByMonitorProjectUrlList(list);
    }

}
