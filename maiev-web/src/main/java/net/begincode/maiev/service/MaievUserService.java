package net.begincode.maiev.service;

import net.begincode.maiev.enums.DelFlagEnum;
import net.begincode.maiev.mapper.MaievUserMapper;
import net.begincode.maiev.model.MaievUser;
import net.begincode.maiev.model.MaievUserExample;
import net.begincode.maiev.model.MaievUserExample.Criteria;
import net.begincode.maiev.util.ListUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Liu
 */
@Service
public class MaievUserService {

    @Resource
    private MaievUserMapper maievUserMapper;

    public int saveUser(MaievUser maievUser) {
        return maievUserMapper.insertSelective(maievUser);
    }


    public MaievUser getUserByNickname(String nickname) {
        MaievUserExample maievUserExample = new MaievUserExample();
        Criteria criteria = maievUserExample.createCriteria();
        criteria.andNicknameEqualTo(nickname);
        criteria.andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<MaievUser> maievUsers = maievUserMapper.selectByExample(maievUserExample);
        if (ListUtils.isNull(maievUsers)) {
            return null;
        }
        return maievUsers.get(0);
    }

    public MaievUser getUserByUsernameAndPwd(String username, String pwd) {
        MaievUserExample maievUserExample = new MaievUserExample();
        Criteria criteria = maievUserExample.createCriteria();
        criteria.andUsernameEqualTo(username);
        criteria.andPwdEqualTo(pwd);
        criteria.andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<MaievUser> maievUsers = maievUserMapper.selectByExample(maievUserExample);
        if (ListUtils.isNull(maievUsers)) {
            return null;
        }
        return maievUsers.get(0);
    }

    public MaievUser getUserByUserName(String userName) {
        MaievUserExample maievUserExample = new MaievUserExample();
        maievUserExample.createCriteria().andUsernameEqualTo(userName).andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<MaievUser> maievUsers = maievUserMapper.selectByExample(maievUserExample);
        if (ListUtils.isNull(maievUsers)) {
            return null;
        }
        return maievUsers.get(0);
    }

    public MaievUser getUserByEmail(String email) {
        MaievUserExample maievUserExample = new MaievUserExample();
        maievUserExample.createCriteria().andEmailEqualTo(email).andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<MaievUser> maievUsers = maievUserMapper.selectByExample(maievUserExample);
        if (ListUtils.isNull(maievUsers)) {
            return null;
        }
        return maievUsers.get(0);
    }

    public MaievUser getUserById(Integer id){
        MaievUserExample maievUserExample = new MaievUserExample();
        maievUserExample.createCriteria().andIdEqualTo(id).andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<MaievUser> maievUsers = maievUserMapper.selectByExample(maievUserExample);
        if(ListUtils.isNull(maievUsers)){
            return null;
        }
        return maievUsers.get(0);
    }


}
