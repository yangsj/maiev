package net.begincode.maiev.service;

import net.begincode.maiev.mapper.MonitorDetailMapper;
import net.begincode.maiev.model.MonitorDetail;
import net.begincode.maiev.model.MonitorDetailExample;
import net.begincode.maiev.util.ListUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Liu on 2017/1/16.
 */
@Service
public class MonitorDetailService {
    @Resource
    private MonitorDetailMapper monitorDetailMapper;

    /**
     * 根据monitorItemId获得MonitorDetail，如果得到返回MonitorDetail，如果没有返回null
     *
     * @param monitorItemId
     * @return
     */
    public MonitorDetail getMonitorDetailByMonitorItemId(Integer monitorItemId) {
        MonitorDetailExample monitorDetailExample = new MonitorDetailExample();
        MonitorDetailExample.Criteria criteria = monitorDetailExample.createCriteria();
        criteria.andMonitorItemIdEqualTo(monitorItemId);
        List<MonitorDetail> monitorDetails = monitorDetailMapper.selectByExample(monitorDetailExample);
        if (ListUtils.isNull(monitorDetails)) {
            return null;
        }
        return monitorDetails.get(0);
    }

    /**
     * 根据ID更新MonitorDetail的内容
     *
     * @param monitorDetail
     * @return
     */
    public Integer updateMonitorDetail(MonitorDetail monitorDetail) {
        int i = monitorDetailMapper.updateByPrimaryKey(monitorDetail);
        return i;
    }

    /**
     * 保存一个monitorDetail，返回对应的主键ID
     *
     * @param monitorDetail
     * @return
     */
    public Integer saveMonitorDetail(MonitorDetail monitorDetail) {
        monitorDetailMapper.insertSelective(monitorDetail);
        return monitorDetail.getId();
    }

    public MonitorDetail getMonitorDetail(Integer monitorItemId) {
        MonitorDetailExample monitorDetailExample = new MonitorDetailExample();
        monitorDetailExample.createCriteria().andMonitorItemIdEqualTo(monitorItemId);
        List<MonitorDetail> list = monitorDetailMapper.selectByExample(monitorDetailExample);
        if (ListUtils.isNull(list)) {
            return null;
        }
        return list.get(0);
    }

    public List<MonitorDetail> listMonitorDetailInItemIdList(List<Integer> itemIdList){
        MonitorDetailExample monitorDetailExample = new MonitorDetailExample();
        monitorDetailExample.createCriteria().andMonitorItemIdIn(itemIdList);
        return monitorDetailMapper.selectByExample(monitorDetailExample);
    }


}
