package net.begincode.maiev.service;

import net.begincode.maiev.mapper.MonitorGroupItemMapper;
import net.begincode.maiev.model.MonitorGroupItem;
import net.begincode.maiev.model.MonitorGroupItemExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Liu on 2017/1/12.
 */
@Service
public class MonitorGroupItemService {
    @Resource
    private MonitorGroupItemMapper monitorGroupItemMapper;

    /**
     * 根据itemId和groupId删除对应的MonitorGroupItem
     *
     * @param itemId
     * @param groupId
     * @return
     */
    public void deleteMonitorGroupItemByItemIdAndGroupId(Integer itemId, Integer groupId) {
        MonitorGroupItemExample monitorGroupItemExample = new MonitorGroupItemExample();
        MonitorGroupItemExample.Criteria criteria = monitorGroupItemExample.createCriteria();
        criteria.andMonitorItemIdEqualTo(itemId);
        criteria.andGroupIdEqualTo(groupId);
        monitorGroupItemMapper.deleteByExample(monitorGroupItemExample);
    }


    /**
     * 保存一个MonitorGroupItem，如果成功返回主键ID，如果失败返回0
     *
     * @param monitorGroupItem
     * @return
     */
    public void saveMonitorGroupItem(MonitorGroupItem monitorGroupItem) {
        monitorGroupItemMapper.insertSelective(monitorGroupItem);
    }

    /**
     * 查找所有groupid等于参数的MonitorGroupItem，成功返回List<MonitorGroupItem>，没找到返回null
     *
     * @param groupId
     * @return
     */
    public List<MonitorGroupItem> listMonitorGroupItemByGroupId(Integer groupId) {
        MonitorGroupItemExample monitorGroupItemExample = new MonitorGroupItemExample();
        MonitorGroupItemExample.Criteria criteria = monitorGroupItemExample.createCriteria();
        criteria.andGroupIdEqualTo(groupId);
        List<MonitorGroupItem> monitorGroupItems = monitorGroupItemMapper.selectByExample(monitorGroupItemExample);
        if (monitorGroupItems.isEmpty()) {
            return null;
        }
        return monitorGroupItems;
    }
}
