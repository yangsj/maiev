package net.begincode.maiev.service;

import net.begincode.maiev.mapper.MonitorGroupMapper;
import net.begincode.maiev.model.MonitorGroup;
import net.begincode.maiev.model.MonitorGroupExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Liu on 2017/1/12.
 * /**
 */
@Service
public class MonitorGroupService {

    @Resource
    private MonitorGroupMapper monitorGroupMapper;

    /**
     * 根据MonitorGroup的ID查找MonitorGroup，找到返回MonitorGroup，找不到返回null
     *
     * @param id
     * @return
     */
    public MonitorGroup getMonitorGroupById(Integer id) {
        return monitorGroupMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据groupName查找MonitorGroup，如果找到返回对象，如果找不到返回null
     *
     * @param groupName
     * @return
     */
    public MonitorGroup getMonitorGroupByGroupName(String groupName) {
        MonitorGroupExample monitorGroupExample = new MonitorGroupExample();
        MonitorGroupExample.Criteria criteria = monitorGroupExample.createCriteria();
        criteria.andGroupNameEqualTo(groupName);
        List<MonitorGroup> monitorGroups = monitorGroupMapper.selectByExample(monitorGroupExample);
        if (monitorGroups.isEmpty()) {
            return null;
        }
        return monitorGroups.get(0);
    }

    /**
     * 根据项目ID查找所有的分组,如果找到返回List<MonitorGroup>，如果找不到返回null
     *
     * @param projectId
     * @return
     */
    public List<MonitorGroup> listMonitorGroupByProjectId(Integer projectId) {
        MonitorGroupExample monitorGroupExample = new MonitorGroupExample();
        MonitorGroupExample.Criteria criteria = monitorGroupExample.createCriteria();
        criteria.andProjectIdEqualTo(projectId);
        List<MonitorGroup> monitorGroups = monitorGroupMapper.selectByExample(monitorGroupExample);
        if (monitorGroups.isEmpty()) {
            return null;
        }
        return monitorGroups;
    }

    /**
     * 向数据库中保存一个新的MonitorGroup，成功返回插入数据的主键，失败返回null
     *
     * @param monitorGroup
     */
    public void saveMonitorGroup(MonitorGroup monitorGroup) {
        monitorGroupMapper.insertSelective(monitorGroup);
    }

    /**
     * 根据ID删除对应的group，成功返回1，失败返回0
     *
     * @param groupId
     * @return
     */
    public void deleteMonitorGroupById(Integer groupId) {
        monitorGroupMapper.deleteByPrimaryKey(groupId);
    }


    /**
     * 传入项目id 集合 计算分组总数
     *
     * @param projectIdList
     * @return
     */
    public Integer countGroupByProjectIdList(List<Integer> projectIdList) {
        MonitorGroupExample monitorGroupExample = new MonitorGroupExample();
        monitorGroupExample.createCriteria().andProjectIdIn(projectIdList);
        return monitorGroupMapper.countByExample(monitorGroupExample);
    }

    public void updateGroup(MonitorGroup monitorGroup) {
        monitorGroupMapper.updateByPrimaryKeySelective(monitorGroup);
    }

    public List<MonitorGroup> listGroupNameByIds(List<Integer> ids) {
        MonitorGroupExample monitorGroupExample = new MonitorGroupExample();
        monitorGroupExample.createCriteria().andProjectIdIn(ids);
        return monitorGroupMapper.selectByExample(monitorGroupExample);
    }

    /**
     *
     * @param projectId  项目标识
     * @return  监控组的数量
     */
    public Integer countGroupByProjectId(Integer projectId) {
        MonitorGroupExample monitorGroupExample = new MonitorGroupExample();
        monitorGroupExample.createCriteria().andProjectIdEqualTo(projectId);
        return monitorGroupMapper.countByExample(monitorGroupExample);
    }


}
