package net.begincode.maiev.service;

import net.begincode.maiev.mapper.MonitorItemMapper;
import net.begincode.maiev.model.MonitorItem;
import net.begincode.maiev.model.MonitorItemExample;
import net.begincode.maiev.util.ListUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Liu on 2017/1/13.
 */
@Service
public class MonitorItemService {
    @Resource
    private MonitorItemMapper monitorItemMapper;

    /**
     * 添加监控项，返回添加成功后的主键id
     *
     * @param monitorItem
     * @return
     */
    public void saveMonitorItem(MonitorItem monitorItem) {
        monitorItemMapper.insertSelective(monitorItem);
    }

    /**
     * 根据项目ID查对应的监控项item，如果找不到返回null，如果找到返回List<MonitorItem>
     *
     * @param projectId
     * @return
     */
    public List<MonitorItem> listMonitorItemByProjectId(Integer projectId) {
        MonitorItemExample monitorItemExample = new MonitorItemExample();
        MonitorItemExample.Criteria criteria = monitorItemExample.createCriteria();
        criteria.andProjectIdEqualTo(projectId);
        List<MonitorItem> monitorItems = monitorItemMapper.selectByExample(monitorItemExample);
        if (monitorItems.isEmpty()) {
            return null;
        }
        return monitorItems;
    }

    /**
     * 根据ID查找MonitorItem，找到返回MonitorItem，找不到返回null
     *
     * @param id
     * @return
     */
    public MonitorItem getMonitorItemById(Integer id) {
        return monitorItemMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据itemName和projectId查找对应的MonitorItem，找到返回MonitorItem，找不到返回null
     *
     * @param MonitorItemName
     * @param projectId
     * @return
     */
    public MonitorItem getMonitorItemByItemNameAndProjectId(String MonitorItemName, Integer projectId) {
        MonitorItemExample monitorItemExample = new MonitorItemExample();
        MonitorItemExample.Criteria criteria = monitorItemExample.createCriteria();
        criteria.andMonitorItemNameEqualTo(MonitorItemName);
        criteria.andProjectIdEqualTo(projectId);
        List<MonitorItem> monitorItems = monitorItemMapper.selectByExample(monitorItemExample);
        if (monitorItems.isEmpty()) {
            return null;
        }
        return monitorItems.get(0);
    }


    /**
     * 通过监控项名字集合  返回集合
     *
     * @param itemNameList
     * @return
     */
    public List<MonitorItem> listMonitorItemByItemName(List<String> itemNameList) {
        MonitorItemExample monitorItemExample = new MonitorItemExample();
        monitorItemExample.createCriteria().andMonitorItemNameIn(itemNameList);
        List<MonitorItem> itemList = monitorItemMapper.selectByExample(monitorItemExample);
        if (ListUtils.isNull(itemList)) {
            return null;
        }
        return itemList;
    }

    /**
     * 通过名字得到MonitorItem实体
     *
     * @param itemName
     * @return
     */
    public MonitorItem getMonitorItemByName(String itemName,Integer projectId) {
        MonitorItemExample monitorItemExample = new MonitorItemExample();
        monitorItemExample.createCriteria().andMonitorItemNameEqualTo(itemName).andProjectIdEqualTo(projectId);
        List<MonitorItem> list = monitorItemMapper.selectByExample(monitorItemExample);
        if (ListUtils.isNull(list)) {
            return null;
        }
        return list.get(0);
    }

    public List<MonitorItem> listMonitorItemWithRowbounds(Integer projectId, Integer currentNum, Integer eachSize) {
        MonitorItemExample monitorItemExample = new MonitorItemExample();
        monitorItemExample.createCriteria().andProjectIdEqualTo(projectId);
        return monitorItemMapper.selectByExampleWithRowbounds(monitorItemExample, new RowBounds((currentNum - 1) * eachSize, eachSize));
    }

    public List<MonitorItem> listMonitorItemByIds(List<Integer> ids){
        MonitorItemExample monitorItemExample = new MonitorItemExample();
        monitorItemExample.createCriteria().andIdIn(ids);
        return monitorItemMapper.selectByExample(monitorItemExample);
    }





}
