package net.begincode.maiev.service;

import net.begincode.maiev.enums.DelFlagEnum;
import net.begincode.maiev.mapper.MonitorProjectMapper;
import net.begincode.maiev.model.MonitorProject;
import net.begincode.maiev.model.MonitorProjectExample;
import net.begincode.maiev.model.MonitorProjectExample.Criteria;

import net.begincode.maiev.util.ListUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

/**
 * Created by Stay on 2016/12/26 13:53.
 */
@Service
public class MonitorProjectService {

    @Resource
    private MonitorProjectMapper monitorProjectMapper;

    /**
     * 查询项目列表
     * 进行分页查询
     * 并对ID进行降序排列
     *
     * @param
     * @return
     */
    public List<MonitorProject> listMonitorProjectWithRowbounds(Integer currentNum, Integer eachSize, Integer userId) {
        MonitorProjectExample monitorProjectExample = new MonitorProjectExample();
        monitorProjectExample.setOrderByClause("id desc");
        Criteria criteria = monitorProjectExample.createCriteria();
        criteria.andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        criteria.andUserIdEqualTo(userId);
        return monitorProjectMapper.selectByExampleWithRowbounds(monitorProjectExample, new RowBounds((currentNum - 1) * eachSize, eachSize));
    }

    /**
     * 得到项目总纪录数
     *
     * @return
     */
    public int countByUserId(Integer userId) {
        MonitorProjectExample monitorProjectExample = new MonitorProjectExample();
        Criteria criteria = monitorProjectExample.createCriteria();
        criteria.andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        criteria.andUserIdEqualTo(userId);
        return monitorProjectMapper.countByExample(monitorProjectExample);
    }


    /**
     * 根据用户ID查找所有的MonitorProject,如果找到返回List<MonitorProject>，如果找不到返回null
     *
     * @param userId
     * @return
     */
    public List<MonitorProject> listMonitorProjectByUserId(Integer userId) {
        MonitorProjectExample monitorProjectExample = new MonitorProjectExample();
        Criteria criteria = monitorProjectExample.createCriteria();
        criteria.andUserIdEqualTo(userId).andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        List<MonitorProject> projectList = monitorProjectMapper.selectByExample(monitorProjectExample);
        return projectList;
    }

    /**
     * 标识集合查找监控项目
     *
     * @param idList
     * @return
     */
    public List<MonitorProject> listMonitorProjectByIdList(List<Integer> idList) {
        MonitorProjectExample monitorProjectExample = new MonitorProjectExample();
        monitorProjectExample.createCriteria().andIdIn(idList).andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode());
        return monitorProjectMapper.selectByExample(monitorProjectExample);
    }

    /**
     * 根据monitorProjectId查找MonitorProject,找到返回MonitorProject，找不到返回null
     *
     * @param monitorProjectId
     * @return
     */
    public MonitorProject getMonitorProjectByProjectId(Integer monitorProjectId) {
        MonitorProject monitorProject = monitorProjectMapper.selectByPrimaryKey(monitorProjectId);
        return monitorProject;
    }


    public Integer updateProjectStatus(MonitorProject monitorProject) {
        return monitorProjectMapper.updateByPrimaryKeySelective(monitorProject);
    }

    public void saveMonitorProject(MonitorProject monitorProject) {
        monitorProjectMapper.insertSelective(monitorProject);
    }

    /**
     * 更改项目删除状态
     *
     * @param userId
     * @param projectId
     */
    public void deleteMonitorProjectByUserIdAndProId(Integer userId, Integer projectId) {
        MonitorProjectExample monitorProjectExample = new MonitorProjectExample();
        monitorProjectExample.createCriteria().andUserIdEqualTo(userId).andIdEqualTo(projectId);
        MonitorProject monitorProject = new MonitorProject();
        monitorProject.setDeleteFlag(DelFlagEnum.IS_DEL.getCode());
        monitorProjectMapper.updateByExampleSelective(monitorProject, monitorProjectExample);
    }

    public void updateMonitorProject(MonitorProject monitorProject, Integer projectId, Integer userId) {
        MonitorProjectExample monitorProjectExample = new MonitorProjectExample();
        monitorProjectExample.createCriteria().andIdEqualTo(projectId).andUserIdEqualTo(userId);
        monitorProjectMapper.updateByExampleSelective(monitorProject, monitorProjectExample);
    }


    public List<MonitorProject> getProjectByUserId(Integer userId) {
        MonitorProjectExample monitorProjectExample = new MonitorProjectExample();
        monitorProjectExample.createCriteria().andDeleteFlagEqualTo(DelFlagEnum.NO_DEL.getCode())
                .andUserIdEqualTo(userId);
        return monitorProjectMapper.selectByExample(monitorProjectExample);
    }
}
