package net.begincode.maiev.service;

import net.begincode.maiev.mapper.MonitorProjectUrlMapper;
import net.begincode.maiev.model.MonitorProjectUrl;
import net.begincode.maiev.model.MonitorProjectUrlExample;
import net.begincode.maiev.model.MonitorProjectUrlExample.Criteria;
import net.begincode.maiev.util.ListUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Liu on 2017/1/12.
 */
@Service
public class MonitorProjectUrlService {

    @Resource
    private MonitorProjectUrlMapper monitorProjectUrlMapper;

    /**
     * 根据项目ID返回对应的监控URL，如果找到返回List<MonitorProjectUrl>，找不到返回空集合List<MonitorProjectUrl>
     *
     * @param projectId
     * @return
     */
    public List<MonitorProjectUrl> listMonitorProjectUrlByProjectId(Integer projectId) {
        MonitorProjectUrlExample monitorProjectUrlExample = new MonitorProjectUrlExample();
        MonitorProjectUrlExample.Criteria criteria = monitorProjectUrlExample.createCriteria();
        criteria.andProjectIdEqualTo(projectId);
        List<MonitorProjectUrl> monitorProjectUrls = monitorProjectUrlMapper.selectByExample(monitorProjectUrlExample);
        return monitorProjectUrls;
    }

    /**
     * 根据项目ID删除monitorUrls中所有的monitorUrl
     *
     * @param projectId
     * @param monitorUrls
     */
    public void deleteMonitorProjectUrlsByProjectId(Integer projectId, List<String> monitorUrls) {
        MonitorProjectUrlExample monitorProjectUrlExample = new MonitorProjectUrlExample();
        MonitorProjectUrlExample.Criteria criteria = monitorProjectUrlExample.createCriteria();
        criteria.andProjectIdEqualTo(projectId);
        criteria.andMonitorUrlIn(monitorUrls);
        monitorProjectUrlMapper.deleteByExample(monitorProjectUrlExample);
    }

    /**
     * 保存monitorProjectUrl
     *
     * @param monitorProjectUrl
     */
    public void saveMonitorUrl(MonitorProjectUrl monitorProjectUrl) {
        monitorProjectUrlMapper.insertSelective(monitorProjectUrl);
    }

    /**
     * 根据项目标志删除url
     */
    public int removeProjectUrlByProjectId(List<Integer> projectIds) {
        MonitorProjectUrlExample monitorProjectUrlExample = new MonitorProjectUrlExample();
        Criteria criteria = monitorProjectUrlExample.createCriteria();
        criteria.andIdIn(projectIds);
        return monitorProjectUrlMapper.deleteByExample(monitorProjectUrlExample);
    }

    /**
     * 传入项目id list集合 返回项目url对应的总数
     *
     * @param projectIdList
     * @return
     */
    public Integer countProjectUrl(List<Integer> projectIdList) {
        MonitorProjectUrlExample monitorProjectUrlExample = new MonitorProjectUrlExample();
        monitorProjectUrlExample.createCriteria().andProjectIdIn(projectIdList);
        return monitorProjectUrlMapper.countByExample(monitorProjectUrlExample);
    }


    /**
     * 返回所有url集合
     *
     * @return
     */
    public List<MonitorProjectUrl> listUrl() {
        return monitorProjectUrlMapper.selectByExample(new MonitorProjectUrlExample());
    }

    public void removeProjectUrlByProId(Integer projectId) {
        MonitorProjectUrlExample monitorProjectUrlExample = new MonitorProjectUrlExample();
        monitorProjectUrlExample.createCriteria().andProjectIdEqualTo(projectId);
        monitorProjectUrlMapper.deleteByExample(monitorProjectUrlExample);
    }

    public MonitorProjectUrl getProjectUrlByName(String urlName) {
        MonitorProjectUrlExample monitorProjectUrlExample = new MonitorProjectUrlExample();
        monitorProjectUrlExample.createCriteria().andMonitorUrlEqualTo(urlName);
        List<MonitorProjectUrl> list = monitorProjectUrlMapper.selectByExample(monitorProjectUrlExample);
        if (ListUtils.isNull(list)) {
            return null;
        }
        return list.get(0);
    }

    public Integer removeProjectUrlByName(String urlName,Integer projectId) {
        MonitorProjectUrlExample monitorProjectUrlExample = new MonitorProjectUrlExample();
        monitorProjectUrlExample.createCriteria().andMonitorUrlEqualTo(urlName).andProjectIdEqualTo(projectId);
        return monitorProjectUrlMapper.deleteByExample(monitorProjectUrlExample);
    }


}
