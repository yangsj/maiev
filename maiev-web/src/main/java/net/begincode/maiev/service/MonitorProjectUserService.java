package net.begincode.maiev.service;

import net.begincode.maiev.mapper.MonitorProjectUserMapper;
import net.begincode.maiev.model.MonitorProjectUser;
import net.begincode.maiev.model.MonitorProjectUserExample;
import net.begincode.maiev.util.ListUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * Created by think on 2017/1/9.
 */
@Service
public class MonitorProjectUserService {

    @Resource
    private MonitorProjectUserMapper monitorProjectUserMapper;


    /**
     * 插入多条项目管理人员
     *
     * @param userIds,projectId
     * @return
     */
    public void insert(Integer[] userIds, Integer projectId) {
        for (int i = 0; i < userIds.length; i++) {
            MonitorProjectUser monitorProjectUser = new MonitorProjectUser();
            monitorProjectUser.setMonitorProjectId(projectId);
            monitorProjectUser.setUserId(userIds[i]);
            monitorProjectUserMapper.insert(monitorProjectUser);
        }
    }

    /**
     * 根据ProjectId清除所有项目管理人员
     *
     * @param projectId
     * @return
     */
    public void deleteByProjectIdAndUserId(Integer projectId,Integer userId) {
        MonitorProjectUserExample monitorProjectUserExample = new MonitorProjectUserExample();
        monitorProjectUserExample.createCriteria().andUserIdEqualTo(userId).andMonitorProjectIdEqualTo(projectId);
        monitorProjectUserMapper.deleteByExample(monitorProjectUserExample);
    }

    /**
     * 根据主键删除项目管理人员
     *
     * @param ids
     * @return
     */
    public void deleteByUserIdAndProjectId(Integer[] ids, Integer projectId) {
        List<Integer> idsList = Arrays.asList(ids);
        MonitorProjectUserExample projectUserExample = new MonitorProjectUserExample();
        MonitorProjectUserExample.Criteria criteria = projectUserExample.createCriteria();
        criteria.andMonitorProjectIdEqualTo(projectId);
        criteria.andUserIdIn(idsList);
        monitorProjectUserMapper.deleteByExample(projectUserExample);
    }

    /**
     * 根据projectID分页查询
     *
     * @param projectId,pageParam
     * @return
     */
    public List<MonitorProjectUser> selectByProjectId(Integer projectId, int currentNum, int pageEachSize) {
        MonitorProjectUserExample projectUserExample = new MonitorProjectUserExample();
        MonitorProjectUserExample.Criteria criteria = projectUserExample.createCriteria();
        if (!projectId.equals(0) && projectId != null) {
            criteria.andMonitorProjectIdEqualTo(projectId);
        }
        List<MonitorProjectUser> list = monitorProjectUserMapper.selectByExampleWithRowbounds(projectUserExample, new RowBounds((currentNum - 1) * pageEachSize, pageEachSize));
        return list;
    }

    /**
     * 根据projectID查询所有
     *
     * @param projectId,pageParam
     * @return
     */
    public List<MonitorProjectUser> selectByProjectId(Integer projectId) {
        MonitorProjectUserExample projectUserExample = new MonitorProjectUserExample();
        MonitorProjectUserExample.Criteria criteria = projectUserExample.createCriteria();
        if (!projectId.equals(0) && projectId != null) {
            criteria.andMonitorProjectIdEqualTo(projectId);
        }
        List<MonitorProjectUser> list = monitorProjectUserMapper.selectByExample(projectUserExample);
        return list;
    }

    /**
     * 查询总条数
     *
     * @return
     */
    public int selectTotal() {
        MonitorProjectUserExample projectUserExample = new MonitorProjectUserExample();
        return monitorProjectUserMapper.countByExample(projectUserExample);
    }

    /**
     * 根据Id查询
     *
     * @param id
     * @return
     */
    public MonitorProjectUser selectByPrimaryKey(Integer id) {
        return monitorProjectUserMapper.selectByPrimaryKey(id);
    }


    /**
     * 通过userId查找所有MonitorProjectUser
     *
     * @param maievUserId
     * @return
     */
    public List<MonitorProjectUser> listMonitorProjectUserByUserId(Integer maievUserId,Integer currentNum, Integer eachSize) {
        MonitorProjectUserExample monitorProjectUserExample = new MonitorProjectUserExample();
        MonitorProjectUserExample.Criteria criteria = monitorProjectUserExample.createCriteria();
        criteria.andUserIdEqualTo(maievUserId);
        List<MonitorProjectUser> monitorProjectUserList = monitorProjectUserMapper.selectByExampleWithRowbounds(monitorProjectUserExample,new RowBounds((currentNum - 1) * eachSize, eachSize));
        if (ListUtils.isNull(monitorProjectUserList)) {
            return null;
        }
        return monitorProjectUserList;
    }

    /**
     * 添加项目管理人员
     *
     * @param monitorProjectUser
     * @return
     */
    public int createProjectUser(MonitorProjectUser monitorProjectUser) {
        return monitorProjectUserMapper.insert(monitorProjectUser);
    }





}
