package net.begincode.maiev.service;

import net.begincode.maiev.enums.HandleFlagEnum;
import net.begincode.maiev.mapper.BizMonitorSummaryMapper;
import net.begincode.maiev.mapper.MonitorSummaryMapper;
import net.begincode.maiev.model.MonitorSummary;
import net.begincode.maiev.model.MonitorSummaryExample;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Stay on 2017/2/28  15:25.
 */
@Service
public class MonitorSummaryService {

    private Logger logger = LoggerFactory.getLogger(MonitorSummaryService.class);

    @Resource
    private BizMonitorSummaryMapper bizMonitorSummaryMapper;
    @Resource
    private MonitorSummaryMapper monitorSummaryMapper;

    /**
     * 批量插入数据
     *
     * @param summaryList
     * @return
     */
    public Integer saveBatchList(List<MonitorSummary> summaryList) {
        return bizMonitorSummaryMapper.insertBatchListMonitorSummary(summaryList);
    }

    public Integer countMonitorSummaryByDate(Date date,List<Integer> list) {
        MonitorSummaryExample monitorSummaryExample = new MonitorSummaryExample();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date afterDate = dateFormat.parse(dateFormat.format(date.getTime() - 60000));
            monitorSummaryExample.createCriteria().andCreateDateBetween(date, afterDate).andProjectIdIn(list);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return monitorSummaryMapper.countByExample(monitorSummaryExample);
    }
    /**
     * 查询指定大小未处理的数据出来
     *
     * @param size
     * @return
     */
    public List<MonitorSummary> listNoHandleSummaryBySize(Integer size) {
        MonitorSummaryExample monitorSummaryExample = new MonitorSummaryExample();
        monitorSummaryExample.createCriteria().andHandleFlagEqualTo(HandleFlagEnum.NO_HANDLE.getCode());
        return monitorSummaryMapper.selectByExampleWithRowbounds(monitorSummaryExample, new RowBounds(0, size));
    }

    /**
     * 查询当前时间两分钟前未处理的数据
     *
     * @return
     */
    public List<MonitorSummary> listMinuteAgoNoHandleSumary() {
        MonitorSummaryExample monitorSummaryExample = new MonitorSummaryExample();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -2);
        Date minuteAgo = calendar.getTime();
        monitorSummaryExample.createCriteria().andHandleFlagEqualTo(HandleFlagEnum.NO_HANDLE.getCode()).andCreateDateBetween(minuteAgo, new Date());
        return monitorSummaryMapper.selectByExample(monitorSummaryExample);
    }


    /**
     * 根据id号 批量更新handle_flag 0 变成 1
     *
     * @param list
     * @return
     */
    public Integer updateHandleSummaryByIdList(List<Integer> list) {
        return bizMonitorSummaryMapper.updateBatchHandleByIdList(list);
    }

    /**
     * 根据实体集合 批量更新handle_flag 0 变成 1
     *
     * @param list
     * @return
     */
    public Integer updateHandleSummaryByList(List<MonitorSummary> list) {
        return bizMonitorSummaryMapper.updateBatchHandleByList(list);
    }



}
