package net.begincode.maiev.service;

import net.begincode.maiev.mapper.ProjectOverviewMapper;
import net.begincode.maiev.model.ProjectOverview;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Stay on 2017/3/12  16:46.
 */
@Service
public class ProjectOverviewService {
    @Resource
    private ProjectOverviewMapper projectOverviewMapper;


    public List<ProjectOverview> listProjectOverviewByPage(Integer current,Integer pageSize){
        return projectOverviewMapper.listByPage(current,pageSize);
    }



}
