package net.begincode.maiev.session;

import net.begincode.maiev.model.MaievUser;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Stay on 2017/2/24  23:49.
 */
public class SessionOperation {
    @Resource
    private static HttpSession httpSession;

    public static MaievUser getUser(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (MaievUser) request.getSession().getAttribute("loginUser");
    }

    public static void loginOut(){
        httpSession.setAttribute("loginUser", null);
    }



}
