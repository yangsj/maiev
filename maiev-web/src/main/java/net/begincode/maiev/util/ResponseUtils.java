package net.begincode.maiev.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.begincode.maiev.bean.Response;
import net.begincode.maiev.enums.ResponseEnum;
import org.slf4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Stay on 2017/3/4  13:28.
 */
public class ResponseUtils {

    private ResponseUtils(){}

    public static void exceptionResponse(Logger logger,ResponseEnum status) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        response.setHeader("Content-Type", "text/html;charset=UTF-8");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(response.getOutputStream(), Response.failed(status));
        } catch (IOException e) {
            logger.error("Response error", e);
        }
    }


}
