/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.40 : Database - maiev
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`maiev` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `maiev`;

/*Table structure for table `alarm_record` */

DROP TABLE IF EXISTS `alarm_record`;

CREATE TABLE `alarm_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户标识',
  `alarm_content` varchar(300) NOT NULL DEFAULT '' COMMENT '告警内容（文本即可）',
  `read_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '已读标识（0未读，1已读）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='用户告警';

/*Data for the table `alarm_record` */

/*Table structure for table `alarm_set` */

DROP TABLE IF EXISTS `alarm_set`;

CREATE TABLE `alarm_set` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `time_threshold` int(11) NOT NULL DEFAULT '0' COMMENT '超时报警阈值',
  `qps_threshold` int(11) NOT NULL DEFAULT '0' COMMENT 'qps报警阈值',
  `monitor_item_id` int(11) NOT NULL DEFAULT '0' COMMENT 'url标识',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '分组标识',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户标识',
  `delete_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识（0未删除，1已删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='告警设置';

/*Data for the table `alarm_set` */

/*Table structure for table `maiev_user` */

DROP TABLE IF EXISTS `maiev_user`;

CREATE TABLE `maiev_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `nickname` varchar(128) NOT NULL DEFAULT '' COMMENT '昵称',
  `username` varchar(128) NOT NULL DEFAULT '' COMMENT '账号',
  `pwd` varchar(128) NOT NULL DEFAULT '' COMMENT '密码',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `delete_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识（0未删除，1已删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `maiev_user` */

/*Table structure for table `monitor_detail` */

DROP TABLE IF EXISTS `monitor_detail`;

CREATE TABLE `monitor_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `ip` varchar(128) NOT NULL DEFAULT '' COMMENT 'IP',
  `request_type` varchar(32) NOT NULL DEFAULT '' COMMENT '请求类型',
  `request_count` int(11) NOT NULL DEFAULT '0' COMMENT '执行次数',
  `request_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '请求总时间',
  `avg_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '平均执行一次的时间',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '数据更新时间',
  `monitor_item_id` int(11) NOT NULL DEFAULT '0' COMMENT '监控项标识',
  PRIMARY KEY (`id`),
  KEY `index_item_id` (`monitor_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COMMENT='监控详细';

/*Data for the table `monitor_detail` */

/*Table structure for table `monitor_group` */

DROP TABLE IF EXISTS `monitor_group`;

CREATE TABLE `monitor_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `group_name` varchar(32) NOT NULL DEFAULT '' COMMENT '分组名',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `project_id` int(11) NOT NULL DEFAULT '0' COMMENT '项目标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='分组';

/*Data for the table `monitor_group` */

/*Table structure for table `monitor_group_item` */

DROP TABLE IF EXISTS `monitor_group_item`;

CREATE TABLE `monitor_group_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `monitor_item_id` int(11) NOT NULL DEFAULT '0' COMMENT '监控项标识',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '分组标识',
  PRIMARY KEY (`id`),
  KEY `index_item_id` (`monitor_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='分组对应监控项';

/*Data for the table `monitor_group_item` */

/*Table structure for table `monitor_item` */

DROP TABLE IF EXISTS `monitor_item`;

CREATE TABLE `monitor_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `monitor_item_name` varchar(200) DEFAULT '' COMMENT 'url名',
  `project_name` varchar(128) NOT NULL DEFAULT '' COMMENT '项目名',
  `project_id` int(11) NOT NULL DEFAULT '0' COMMENT '项目标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='监控项（URL）';

/*Data for the table `monitor_item` */

/*Table structure for table `monitor_project` */

DROP TABLE IF EXISTS `monitor_project`;

CREATE TABLE `monitor_project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `project_name` varchar(128) NOT NULL DEFAULT '' COMMENT '项目名',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户标识',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `project_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '监控状态(0未知,1正常,2异常)',
  `note` varchar(200) NOT NULL DEFAULT '' COMMENT '项目备注',
  `delete_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识（0未删除，1已删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='监控项目';

/*Data for the table `monitor_project` */

/*Table structure for table `monitor_project_url` */

DROP TABLE IF EXISTS `monitor_project_url`;

CREATE TABLE `monitor_project_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `monitor_url` varchar(300) NOT NULL DEFAULT '' COMMENT '监控抓取url',
  `project_id` int(11) NOT NULL DEFAULT '0' COMMENT '项目标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='监控抓取url';

/*Data for the table `monitor_project_url` */

/*Table structure for table `monitor_project_user` */

DROP TABLE IF EXISTS `monitor_project_user`;

CREATE TABLE `monitor_project_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '标识',
  `monitor_project_id` int(11) NOT NULL DEFAULT '0' COMMENT '监控项目标识',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='项目管理人员';

/*Data for the table `monitor_project_user` */

/*Table structure for table `monitor_summary` */

DROP TABLE IF EXISTS `monitor_summary`;

CREATE TABLE `monitor_summary` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长id',
  `monitor_name` varchar(128) NOT NULL DEFAULT '' COMMENT '监控类名',
  `max_request_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '最大请求时间',
  `total_request_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '请求总时间',
  `request_type` varchar(32) NOT NULL DEFAULT '' COMMENT '请求类型',
  `monitor_host` varchar(128) NOT NULL DEFAULT '' COMMENT '客户端ip',
  `create_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '数据创建时间',
  `request_count` int(11) NOT NULL DEFAULT '0' COMMENT '请求总次数',
  `project_id` int(11) NOT NULL COMMENT '项目标识',
  `handle_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据处理标识(0未处理,1已处理)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=689 DEFAULT CHARSET=utf8 COMMENT='监控汇总数据';

/*Data for the table `monitor_summary` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
