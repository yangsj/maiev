/**
 * Created by Stay on 2017/4/25.
 */
var selectedProjectId;
$(document).ready(function () {

    selectProject();

    pageAlarm();

});
function selectProject() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: ctx + "/alarm/project.htm",
        success: function (data) {
            if (data.code == 0) {
                var select = "";
                $.each(data.data, function (i) {
                    select = "<option id='" + data.data[i].id + "'>" + data.data[i].projectName + "</option>";
                    $("#selectProject").append(select);
                });
                selectedProjectId = $("#selectProject option:selected")[0].id;
            } else {
                showModel(data.msg);
            }
        }
    });
}
function radioCheck() {
    $("input[name='checkAlarm']").each(function () {
        if ($(this)[0].checked == true) {
            if ($(this)[0].id == "selectGroup") {
                $("#group").removeAttr("disabled");
                groupSelect(selectedProjectId);
                $("#item").attr("disabled", "");
                $("#item").empty();
            }
            if ($(this)[0].id == "selectItem") {
                $("#item").removeAttr("disabled");
                itemSelect(selectedProjectId);
                $("#group").attr("disabled", "");
                $("#group").empty();
            }
        }
    });
}

function projectOptionChange() {
    selectedProjectId = $("#selectProject option:selected")[0].id;
    $("input[name='checkAlarm']").each(function () {
        if ($(this)[0].checked == true) {
            if ($(this)[0].id == "selectGroup") {
                $("#group").empty();
                groupSelect(selectedProjectId);
            }
            if ($(this)[0].id == "selectItem") {
                $("#item").empty();
                itemSelect(selectedProjectId);
            }
        }
    });
}


function groupSelect(projectId) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: ctx + "/alarm/group/" + projectId + ".htm",
        success: function (data) {
            if (data.code == 0) {
                var select = "";
                $.each(data.data, function (i) {
                    select = "<option id='" + data.data[i].id + "'>" + data.data[i].groupName + "</option>";
                    $("#group").append(select);
                });
            } else {
                showModel(data.msg);
            }
        }
    });
}

function itemSelect(projectId) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: ctx + "/alarm/item/" + projectId + ".htm",
        success: function (data) {
            if (data.code == 0) {
                var select = "";
                $.each(data.data, function (i) {
                    select = "<option id='" + data.data[i].id + "'>" + data.data[i].itemName + "</option>";
                    $("#item").append(select);
                });
            } else {
                showModel(data.msg);
            }
        }
    });
}

function alarmSetSubmit() {
    if ($("#group option:selected")[0] != null) {
        $.ajax({
            type: "POST",
            dataType: "json",
            data: {
                "alarmSet.timeThreshold": $("#time_threshold").val(),
                "alarmSet.qpsThreshold": $("#qps_threshold").val(),
                "alarmSet.groupId": $("#group option:selected")[0].id,
            },
            url: ctx + "/alarm/save.htm",
            success: function (data) {
                if (data.code == 0) {
                    showModel(data.msg);
                    $("#qps_threshold").val("");
                    $("#time_threshold").val("");
                    pageAlarm();
                } else {
                    showModel(data.msg);
                }
            }
        })
    } else {
        $.ajax({
            type: "POST",
            dataType: "json",
            data: {
                "alarmSet.timeThreshold": $("#time_threshold").val(),
                "alarmSet.qpsThreshold": $("#qps_threshold").val(),
                "alarmSet.monitorItemId": $("#item option:selected")[0].id,
            },
            url: ctx + "/alarm/save.htm",
            success: function (data) {
                if (data.code == 0) {
                    showModel(data.msg);
                    $("#qps_threshold").val("");
                    $("#time_threshold").val("");
                    pageAlarm();
                } else {
                    showModel(data.msg);
                }
            }
        })
    }
}

function pageAlarm() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: ctx + "/alarm/list.htm?page=1",
        success: function (data) {
            if (data.code == 0) {
                alarmTableShow(data);
                pagination(data.data);
            }
        }
    })
}
function pagination(data) {
    $("#paginationId").empty();
    $("#paginationId").jqPaginator({
        totalCounts: data.totalNum,
        pageSize: data.pageEachSize,
        visiblePages: 4,
        first: '<li class="first"><a href="javascript:void(0);">首页<\/a><\/li>',
        prev: '<li class="prev"><a href="javascript:void(0);"><i class="arrow arrow2"><\/i>上一页<\/a><\/li>',
        next: '<li class="next"><a href="javascript:void(0);"><i class="arrow arrow3"><\/i>下一页<\/a><\/li>',
        last: '<li class="last"><a href="javascript:void(0);">末页<\/a><\/li>',
        page: '<li class="page"><a href="javascript:void(0);">{{page}}<\/a><\/li>',
        activeClass: 'active',
        onPageChange: function (currentPage, type) {
            if (type == "change") {
                $.ajax({
                    type: "GET",
                    url: ctx + "/alarm/list.htm?page=" + currentPage,
                    dataType: "json",
                    success: function (data) {
                        alarmTableShow(data);
                    }
                });
            }
        }
    })
}

function alarmTableShow(data) {
    var alarmList = data.data.data;
    $("#alarmDetail").empty();
    var alarmTitle = " <tbody> <tr><th>item名 or 分组名</th>  <th>报警超时阈值(ms)</th> <th>qps报警阈值(次数)</th> <th>操作</th> </tr>";
    $("#alarmDetail").append(alarmTitle);
    var alarm = "";
    $.each(alarmList, function (i) {
        alarm = "<tr><td><span class='label label-success'>"
            + alarmList[i].alarmName + "</span></td><td>" + alarmList[i].timeThreshold + "</td><td>"
            + alarmList[i].qpsThreshold + "</td><td>"
            + "<button type='button' class='btn btn-info' onclick='showUpdateAlarm(" + alarmList[i].id + ")'>修改</button><button type='button' onclick='deleteAlarm(" + alarmList[i].id + ")' class='btn btn-danger'>删除</button></td> </tr>";
        $("#alarmDetail").append(alarm);
    });
    $("#alarmDetail").append("</tbody>");
}
var alarmSetId = "";
function showUpdateAlarm(id) {
    $("#alarmUpdate").modal("show");
    $.ajax({
        type: "GET",
        url: ctx + "/alarm/" + id + ".htm",
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                alarmSetId = data.data.id;
                $("#alarmName").val(data.data.alarmName);
                $("#timeAlarm").val(data.data.timeThreshold);
                $("#qpsAlarm").val(data.data.qpsThreshold);
            } else {
                showModel(data.msg);
            }
        }
    })
}
function updateAlarm() {
    $.ajax({
        type: "POST",
        url: ctx + "/alarm/update.htm",
        dataType: "json",
        data: {
            "alarmSet.id": alarmSetId,
            "alarmSet.timeThreshold": $("#timeAlarm").val(),
            "alarmSet.qpsThreshold": $("#qpsAlarm").val(),
        },
        success: function (data) {
            if (data.code == 0) {
                showModel(data.msg);
                $("#alarmUpdate").modal("hide");
                pageAlarm();
            } else {
                showModel(data.msg);
            }
        }
    });
}

function deleteAlarm(id) {
    var message = confirm("你确认删除此告警吗？");
    if(message == true){
        $.ajax({
            type: "GET",
            url: ctx + "/alarm/delete/" + id + ".htm",
            dataType: "json",
            success: function (data) {
                if (data.code == 0) {
                    showModel(data.msg);
                    pageAlarm();
                }else{
                    showModel(data.msg);
                }
            }
        })
    }
}