/**
 * Created by Stay on 2017/3/6.
 */

$(document).ready(function () {
    sidebarProject();
});


function sidebarProject() {
    $.ajax({
        type: "GET",
        url: ctx + "/project/sidebarProject.htm",
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                $("#project").empty();
                $.each(data.data, function (i) {
                    $("#project").append('<li><a href="'
                        + ctx + '/project/'
                        + data.data[i].projectId
                        + '.htm"><i class="fa fa-circle-o"></i> '
                        + data.data[i].projectName
                        + '</a></li>');
                })
            } else {
                showModel(data.msg);
            }
        }
    });
}




