$(document).ready(function () {
    loadProject();

    createProject();

    updateProject();

    $(document).on("click", "#addUpdateUrlButton button", function () {
        $("#updateGrabUrl").append('<input type="url" class="form-control" name="urls" placeholder="url">');
    });
    $(document).on("click", "#addButton button", function () {
        $("#grabUrl").append('<input type="url" class="form-control" name="grabUrlNames" placeholder="url">');
    })
});

function loadProject() {
    $.ajax({
        type: "GET",
        url: ctx + "/projectList.htm?page=1",
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                if (data.data.data == "") {
                    $("#projectTable").append('<div class="callout callout-danger"> <h4>请先添加数据！</h4> <p></p> </div>');
                } else {
                    pagination(data.data);
                    projectShow("projectTable", data.data.data);
                }
            }
        },
        error: function () {
            //
        }
    });
}


function pagination(data) {
    $("#paginationId").empty();
    $("#paginationId").jqPaginator({
        totalCounts: data.totalNum,
        pageSize: data.pageEachSize,
        visiblePages: 4,
        first: '<li class="first"><a href="javascript:void(0);">首页<\/a><\/li>',
        prev: '<li class="prev"><a href="javascript:void(0);"><i class="arrow arrow2"><\/i>上一页<\/a><\/li>',
        next: '<li class="next"><a href="javascript:void(0);"><i class="arrow arrow3"><\/i>下一页<\/a><\/li>',
        last: '<li class="last"><a href="javascript:void(0);">末页<\/a><\/li>',
        page: '<li class="page"><a href="javascript:void(0);">{{page}}<\/a><\/li>',
        activeClass: 'active',
        onPageChange: function (currentPage, type) {
            if (type == "change") {
                $.ajax({
                    type: "GET",
                    url: ctx + "/projectList.htm?page=" + currentPage,
                    dataType: "json",
                    success: function (data) {
                        projectShow("projectTable", data.data.data);
                    }
                });
            }
        }
    })
}

function projectShow(domId, projectList) {
    $("#" + domId).empty();
    var projectTitle = " <tr><th></th> <th>监控项目名</th> <th>创建时间</th> <th>状态</th> <th>项目备注</th> </tr>";
    $("#" + domId).append(projectTitle);
    var project = "";
    $.each(projectList, function (i) {
        project = "<tr><td><input type='radio' name='projectRadio' value='" + projectList[i].id + "' /></td><td><a href='" + ctx + "/project/" + projectList[i].id + ".htm'>" +
            projectList[i].projectName + "</a></td><td>" +
            projectList[i].createTime + "</td><td>" +
            projectStatusShow(projectList[i].projectStatus) + "</td><td>" +
            projectList[i].note + "</td></tr>";
        $("#" + domId).append(project);
    });
}

function projectStatusShow(status) {
    switch (status) {
        case 0:
            return "<span class='badge bg-yellow'>未知</span>";
            break;
        case 1:
            return "<span class='badge bg-blue'>正常</span>";
            break;
        case 2:
            return "<span class='badge bg-red'>异常</span>";
            break;
    }
}

function showCreateModal() {
    $("#createProjectModal").modal("show");
    $("#grabUrl").empty();
}

function createProject() {
    $("#createProject form").submit(function () {
        $.ajax({
            type: "POST",
            url: ctx + "/project/create.htm",
            data: $("#createProjectModal form").serializeArray(),
            dataType: "json",
            success: function (data) {
                if (data.code == 0) {
                    $("#createProjectModal form")[0].reset();
                    $("#createProjectModal").modal("hide");
                    sidebarProject();
                    loadProject();
                } else {
                    showModel(data.msg);
                }
            }
        });
        return false;
    });
}

function projectDelete() {
    $("input[name='projectRadio']").each(function () {
        if ($(this)[0].checked == true) {
            var projectId = $(this)[0].value;
            var message = confirm("你确认删除此项目吗？");
            if (message === true) {
                $.ajax({
                    type: "POST",
                    url: ctx + "/project/delete.htm?projectId=" + projectId,
                    dataType: "json",
                    success: function (data) {
                        if (data.code == 0) {
                            $("#projectTable").empty();
                            $("#createProjectModal form")[0].reset();
                            sidebarProject();
                            loadProject();
                        } else {
                            showModel(data.msg);
                        }
                    }
                })
            }
        }
    });
}
var updateProjectId;
function showUpdateModal() {
    var checkFlag = 0;   //选中标识  1为选中  0为未选中
    $("input[name='projectRadio']").each(function () {
        if ($(this)[0].checked == true) {
            checkFlag = 1;
            updateProjectId = $(this)[0].value;
            $("#updateProjectModal").modal("show");
            $.ajax({
                type: "GET",
                url: ctx + "/url/project/" + updateProjectId + ".htm",
                dataType: "json",
                success: function (data) {
                    if (data.code == 0) {
                        $("#updateGrabUrl").empty();
                        $("#updateGrabUrl").append("<label>待抓取url名</label>");
                        $.each(data.data, function (i) {
                            $("#updateGrabUrl").append("<input type='url' class='form-control' name='urls' placeholder='url' value='" + data.data[i].monitorUrl + "'><button type='button' onclick='javascript:deleteUrl(\"" + data.data[i].monitorUrl + "\")' class='btn-primary'>删除</button>");
                        })
                    } else {
                        showModel(data.msg);
                    }
                }
            });
            $.ajax({
                type: "GET",
                url: ctx + "/project/modal/" + updateProjectId + ".htm",
                dataType: "json",
                success: function (data) {
                    if (data.code == 0) {
                        $("#updateProjectName").empty();
                        $("#updateProjectName").append("<label>项目名</label>");
                        $("#updateProjectNote").empty();
                        $("#updateProjectNote").append("<label >项目备注</label>");
                        $("#updateProjectName").append("<input type='text' class='form-control' name='projectName' placeholder='项目名' value='" + data.data.projectName + "'>");
                        $("#updateProjectNote").append("<input type='text' class='form-control' name='note' placeholder='备注' value='" + data.data.note + "'>");
                    } else {
                        showModel(data.msg);
                    }
                }
            });
        }
    });
    if (checkFlag == 0) {
        showModel("暂未选中数据!");
    }
}

function updateProject() {
    $("#updateProject form").submit(function () {
        $.ajax({
            type: "POST",
            url: ctx + "/project/update.htm?projectId=" + updateProjectId,
            data: $("#updateProject form").serializeArray(),
            dataType: "json",
            success: function (data) {
                if (data.code == 0) {
                    $("#updateProjectModal").modal("hide");
                    sidebarProject();
                    loadProject();
                } else {
                    showModel(data.msg);
                }
            }
        });
        return false;
    });
}
function deleteUrl(monitorUrl) {
    $.ajax({
        type: "POST",
        url: ctx + "/url/delete.htm",
        data: {
            "urlName": monitorUrl,
            "projectId": updateProjectId,
        },
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                showUpdateModal();
            }
        }
    })
}