$(document).ready(function () {
    $("#login").click(function () {
        $("#pwd").val(hex_md5($("#pwd").val()));
        $.ajax({
            data: $("#loginForm").serializeArray(),
            type: "POST",
            url: ctx + "/user/sign.htm",
            dataType: "json",
            success: function (data) {
                if(data.code == 0){
                    window.location.href = ctx +"/";
                }else{
                    showModel(data.msg);
                    $("#pwd").val(null);
                    imgRefresh();
                }
            }
        });
    })
});


function checkLoginForm(msg) {
    if($("#username").val().trim() =="" ||$("#pwd").val().trim() == ""){
        $("#warnMessage").html(msg);
        $("#warnModal").modal("show");
        return false;
    }
}

function imgRefresh() {
    $(".clo-xs-8").attr("src",ctx + "/user/authImage.htm?"+new Date().getTime());
}
