/**
 * Created by Stay on 2017/3/27.
 */
var page = 1;
$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: ctx + "/project/detail/" + getProjectId() + ".htm",
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                paginationUrl("paginationId", data.data);
                showTopTableByName("urlTable", data.data.data);
            } else {
                showModel(data.msg);
            }
        }
    });

    $(document).on('click', '#groupModal .modal-dialog .modal-content .modal-footer button', function () {
        $.ajax({
            type: "POST",
            data: $(".modal-content form[name='formSaveGroup']").serializeArray(),
            url: ctx + "/group/store.htm",
            dataType: "json",
            success: function (data) {
                if (data.code == 0) {
                    $("#groupModal").modal("hide");
                    window.location.href = ctx + "/project/"+ getProjectId() +".htm"
                } else {
                    showModel(data.msg);
                }
            }
        });
    });


});


function getProjectId() {
    var url = location.href;
    var indexNum = url.lastIndexOf("/");
    var projectId = url.substring(indexNum + 1, url.length - 1).split(".")[0];
    return projectId;
}

function showTopTableByName(elementName, data) {
    var table = $("table[name='" + elementName + "']");
    table.empty();
    table.append("<thead><tr> <th>item名</th> <th>ip</th> <th>请求类型</th> <th>执行次数</th> <th>请求总时间(ms)</th> <th>avg(ms)</th> <th>update</th> </tr> </thead>");
    table.append("<tbody>");
    var project = "";
    $.each(data, function (i) {
        project = "<tr><td><a href='javascript:void(0)'>" +
            data[i].monitorName + "</a></td><td>" + data[i].ip + "</td><td>" + data[i].type + "</td><td>" + data[i].requestCount + "</td><td>" + data[i].requestTotalTime + "</td>" +
            "<td>" + data[i].avgTime + "</td><td>" + data[i].updateTime + "</td></tr>";
        table.append(project);
    });
    table.append("</tbody>");
}

function paginationUrl(domId, data) {
    $("#" + domId).empty();
    $("#" + domId).jqPaginator({
        totalCounts: data.totalNum,
        pageSize: data.pageEachSize,
        visiblePages: 4,
        first: '<li class="first"><a href="javascript:void(0);">首页<\/a><\/li>',
        prev: '<li class="prev"><a href="javascript:void(0);"><i class="arrow arrow2"><\/i>上一页<\/a><\/li>',
        next: '<li class="next"><a href="javascript:void(0);"><i class="arrow arrow3"><\/i>下一页<\/a><\/li>',
        last: '<li class="last"><a href="javascript:void(0);">末页<\/a><\/li>',
        page: '<li class="page"><a href="javascript:void(0);">{{page}}<\/a><\/li>',
        activeClass: 'active',
        onPageChange: function (currentPage, type) {
            if (type == "change") {
                $.ajax({
                    type: "GET",
                    url: ctx + "/project/detail/" + getProjectId() + ".htm?page=" + currentPage,
                    dataType: "json",
                    success: function (data) {
                        if (data.code == 0) {
                            showTopTableByName("urlTable", data.data.data);
                        } else {
                            showModel(data.msg);
                        }
                    }
                });
            }
        }
    })
}
var nowShowGroup;
var nowGroupId;
function groupAdd() {
    $.ajax({
        type: "GET",
        url: ctx + "/group/item/" + getProjectId() + ".htm?p=" + page,
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                tableGroupItemShow(data.data);
            } else {
                showModel(data.msg);
            }
        }
    });
    $("#groupModal").modal("show");
}

function tableGroupItemShow(data) {
    var table = $("table[name='groupUrlTable']");
    table.empty();
    table.append("<thead><tr> <th></th><th>url名</th> </tr> </thead>");
    var item = "";
    $.each(data, function (i) {
        item = "<tr><td><input name='itemIdArray' type='checkbox' value='" + data[i].id + "' /></td><td>" + data[i].monitorItemName + "</td></tr>";
        table.append(item);
    });
    table.append("<input type='hidden' name='monitorGroup.projectId' value='" + getProjectId() + "' />");
    page++;
}

function tableGroupItemAdd() {
    $.ajax({
        type: "GET",
        url: ctx + "/group/item/" + getProjectId() + ".htm?p=" + page,
        dataType: "json",
        success: function (data) {
            var table = $("table[name='groupUrlTable']");
            if (data.code == 0) {
                var item = "";
                $.each(data.data, function (i) {
                    item = "<tr><td><input name='itemIdArray' type='checkbox' value='" + data.data[i].id + "' /></td><td>" + data.data[i].monitorItemName + "</td></tr>";
                    table.append(item);
                });
                page++;
            } else {
                showModel(data.msg);
            }
        }
    });
}
var groupId = "";

function groupEditSubmit() {
    $.ajax({
        type: "POST",
        data: {
            "id": groupId,
            "groupName": $("input[name='newGroupName']")[0].value,
        },
        url: ctx + "/group.htm",
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                $("#groupEdit").modal("hide");
                $("input[name='newGroupName']")[0].value = "";
                GroupAjax();
            } else {
                showModel(data.msg);
            }
        }
    });
}

var clickNum = 0;

function ajaxAppendTable() {
    $.ajax({
        type: "GET",
        url: ctx + "/item.htm?groupId=" + nowGroupId + "&projectId=" + getProjectId() + "&p=" + clickNum,
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                var table = $("#groupItemAdd table");
                $.each(data.data, function (i) {
                    table.append("<tr><td><input name='groupAddItem' type='checkbox' value='" + data.data[i].itemId + "'/></td><td>" + data.data[i].monitorUrlName + "</td></tr>");
                });
                clickNum++;
            } else {
                showModel(data.msg);
            }
        },
        error: function () {
            showModel("请检查服务器!");
        }
    })
}


function deleteItem() {
    var itemId = 0;
    $("input[name='itemCheck']").each(function () {
        if ($(this)[0].checked == true) {
            itemId = $(this)[0].value;
        }
    });
    if (itemId == 0) {
        showModel("请选择一个url");
    } else {
        $.ajax({
            type: "POST",
            url: ctx + "/item/delete.htm?groupId=" + nowGroupId + "&itemId=" + itemId,
            dataType: "json",
            success: function (data) {
                if(data.code == 0){
                    showModel(data.msg);
                    groupUrlShow(nowGroupId, nowShowGroup);
                }else{
                    showModel(data.msg);
                }
            }
        });
    }

}









