/**
 * Created by Stay on 2017/6/15.
 */
$(document).ready(function () {
    request("-12", "day")
});

function request(time, timeType) {
    $.ajax({
        type: "GET",
        url: ctx + "/e/describeMonitorData.htm?projectId=" + getProjectId(),
        dataType: "json",
        success: function (data) {
            if (data.code == 0) {
                if (data.data == '') {
                    //empty
                } else {
                    graph(data.data);
                }
            } else {
                showModel(data.msg);
            }
        }
    })
}

function graph(data) {
    var groupList = new Array();
    for (var i = 0; i < data.length; i++) {
        groupList[i] = data[i].groupName;
    }
    if (data.length == 1) {
        $("#topTable").after(div12(groupList));
    } else {
        $("#topTable").after(div6(groupList));
    }
    var doms = document.getElementsByName("brokenLineEcharts");
    for(var i = 0; i < doms.length;i++){
        graphOperation(doms[i],data[i].list);
    }
}


function div6(groupList) {
    var echartHTML = '';
    for (var i = 0; i < parseInt(groupList.length / 2); i++) {
        echartHTML = echartHTML + '<div class="box box-primary">'
            + '<div class="box-header with-border">'
            + '<h3 class="box-title">' + groupList[i] + '分组信息</h3>'
            + '<div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>'
            + '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>'
            + '</div></div>'
            + '<div class="box-body">'
            + ' <div class="chart"><div class="chart tab-pane active" name="brokenLineEcharts"  height="252" width="740" style="position: relative; height: 252px; width:740px;"></div>'
            + '</div></div>'
            + '</div>';
    }
    var outsideDiv1 = ' <div class="col-md-6">' + echartHTML + '</div>';
    echartHTML = "";
    for (var n = parseInt(groupList.length / 2); n < groupList.length; n++) {
        echartHTML = echartHTML + '<div class="box box-primary">'
            + '<div class="box-header with-border">'
            + '<h3 class="box-title">' + groupList[n] + '分组信息</h3>'
            + '<div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>'
            + '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>'
            + '</div></div>'
            + '<div class="box-body">'
            + ' <div class="chart"><div class="chart tab-pane active" name="brokenLineEcharts"  height="252" width="740" style="position: relative; height: 252px; width:740px;"></div>'
            + '</div></div>'
            + '</div>';
    }
    var outsideDiv2 = ' <div class="col-md-6">' + echartHTML + '</div>';
    var outsideDiv = outsideDiv1 + outsideDiv2;
    return outsideDiv;
}

function div12(groupList) {
    var echartHTML = '<div class="box box-primary">'
        + '<div class="box-header with-border">'
        + '<h3 class="box-title">' + groupList[0] + '分组信息</h3>'
        + '<div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>'
        + '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>'
        + '</div></div>'
        + '<div class="box-body">'
        + ' <div class="chart"><div class="chart tab-pane active" name="brokenLineEcharts"  height="504" width="1480" style="position: relative; height:504px; width:1480px;"></div>'
        + '</div></div>'
        + '</div>';
    var outsideDiv = ' <div class="col-md-12">' + echartHTML + '</div>';
    return outsideDiv;
}

function graphOperation(_dom, list) {
    var itemNameList = new Array();
    var xData = new Array();
    var totalData = new Array();
    for (var i = 0; i < list.length; i++) {
        var obj = new Object();
        itemNameList[i] = list[i].monitorItemName;
        obj.name = list[i].monitorItemName;
        obj.type = 'line';
        obj.stack = '请求平均时间';
        obj.data = new Array();
        var monitorList = list[i].monitorList;
        for (var j = 0; j < monitorList.length; j++) {
            xData[j] = monitorList[j].updateTime.split(" ")[1];
            obj.data[j] = monitorList[j].avgTime;
        }
        totalData.push(obj);
    }
    graphShow(_dom, xData, totalData, itemNameList);
}

function graphShow(_dom, xData, data, lineNameList) {
    var myChart = echarts.init(_dom);
    var option = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: lineNameList
        },
        toolbox: {
            show: true,
            feature: {
                mark: {show: true},
                dataView: {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: true,
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: xData
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLabel: {
                    formatter: '{value}'
                }
            }
        ],
        series: data
    };

    myChart.setOption(option);
}
