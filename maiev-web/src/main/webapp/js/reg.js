$(document).ready(function () {
    validate();
});

function imgRefresh() {
    $(".clo-xs-8").attr("src", ctx + "/user/authImage.htm?" + new Date().getTime());
}


function validate() {
    $("form").bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "maievUser.username": {
                message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '用户名不能为空'
                    },
                    stringLength: {
                        min: 4,
                        max: 18,
                        message: '用户名长度必须在4到18位之间'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: '用户名只能包含大写、小写、数字和下划线'
                    },
                    remote: {
                        type: "POST",
                        url: ctx + "/user/check.htm",
                        delay: 1000,
                        data: {
                            "checkName": "username",
                        },
                        message: "用户名已存在"
                    }
                }
            },
            "maievUser.nickname": {
                message: '昵称验证失败',
                validators: {
                    notEmpty: {
                        message: '昵称不能为空'
                    },
                    stringLength: {
                        min: 3,
                        max: 18,
                        message: '昵称长度必须在3到18位之间'
                    },
                    remote: {
                        type: "POST",
                        url: ctx + "/user/check.htm",
                        delay: 1000,
                        data: {
                            "checkName": "nickname",
                        },
                        message: "昵称已存在"
                    }
                }
            },
            "maievUser.pwd": {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    stringLength: {
                        min: 6,
                        max: 18,
                        message: '密码长度必须在6到30位之间'
                    },
                    different: {
                        field: 'maievUser.username',
                        message: '不能和用户名相同'
                    },
                }
            },
            "maievUser.email": {
                validators: {
                    notEmpty: {
                        message: '邮箱不能为空'
                    },
                    emailAddress: {
                        message: '邮箱地址格式有误'
                    },
                    remote: {
                        type: "POST",
                        url: ctx + "/user/check.htm",
                        delay: 1000,
                        data: {
                            "checkName": "email",
                        },
                        message: "邮箱已存在"
                    }
                }
            },
            "reapt-pwd": {
                validators: {
                    notEmpty: {
                        message: '重复密码不能为空'
                    },
                    identical: {
                        field: 'maievUser.pwd',
                        message: '两次密码不一致'
                    },
                    different: {
                        field: 'username',
                        message: '不能和用户名相同'
                    },
                }
            },
            "code": {
                validators: {
                    notEmpty: {
                        message: '验证码不能为空'
                    },
                    stringLength: {
                        min: 4,
                        max: 4,
                        message: '验证码长度为4'
                    },
                    remote: {
                        type: "POST",
                        url: ctx + "/user/check.htm",
                        delay: 1000,
                        data: {
                            "checkName": "code",
                        },
                        message: "验证码不正确"
                    }
                }
            }
        },
    }).on('success.form.bv', function (e) {
        $.ajax({
            url: ctx + "/user/reg.htm",
            type: "post",
            dataType: "json",
            data: $("form").serializeArray(),
            success: function (data) {
                if (data.code == 0) {
                    window.location.href = ctx + "/user/login.htm";
                } else {
                    showModel(data.msg);
                }
            }
        })

        return false;
    });
}

