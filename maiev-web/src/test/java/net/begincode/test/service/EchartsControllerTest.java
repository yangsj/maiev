package net.begincode.test.service;

import net.begincode.maiev.controller.GraphController;
import net.begincode.maiev.handler.UserContext;
import net.begincode.maiev.model.MaievUser;
import net.begincode.maiev.util.MD5Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;

/**
 * Created by Stay on 2017/6/12  18:08.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration(value = "src/main/webapp")
@ContextConfiguration(locations = {"classpath*:spring/springmvc.xml", "classpath*:spring/dataSource.xml", "classpath*:spring/applicationContext-web.xml", "classpath*:mybatis.xml"})
public class EchartsControllerTest {

    @Resource
    private UserContext userContext;
    @Resource
    private GraphController echartsController;


    @Test
    public void testEcharts() {
        MaievUser maievUser = new MaievUser();
        maievUser.setUsername("stay");
        maievUser.setNickname("stay");
        maievUser.setPwd(MD5Utils.md5("ych4327636"));
        userContext.login(maievUser);
        String json = echartsController.echartsShow(41, -6, "day").toString();
        System.out.println(json);
    }


}
