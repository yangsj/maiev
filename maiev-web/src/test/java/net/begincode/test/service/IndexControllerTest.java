package net.begincode.test.service;

import net.begincode.maiev.bean.PageParam;
import net.begincode.maiev.controller.IndexController;
import net.begincode.maiev.model.MaievUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created by Stay on 2017/3/13  14:57.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration(value = "src/main/webapp")
@ContextConfiguration(locations = {"classpath*:spring/springmvc.xml", "classpath*:spring/dataSource.xml", "classpath*:spring/applicationContext-web.xml", "classpath*:mybatis.xml"})
public class IndexControllerTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Resource
    private IndexController indexController;


    @Resource
    private HttpSession session;


    @Test
    public void testIndex() {
        MaievUser user = new MaievUser();
        user.setNickname("stay");
        user.setUsername("stay");
        user.setPwd("ych4327636");
        session.setAttribute("loginUser",user);
        PageParam pageParam = new PageParam();
        pageParam.setPage(0);
        System.out.println(indexController.projectPage(pageParam).toString());
    }


}
